Библиоткеки:

    pip install flask
    pip install flask_cors
    pip install RusPhonetic - только для работы с фонетиками. для запуска серверов не нужен
    pip install pocketsphinx
    pip install pyaudio - только для записи команд с микрофона (можно посылать текствые, на windows не заработало)
    
    если что-то пошло не так
    
    1. попробовать обновить pip 
        pip install --upgrade pip
    2. установить swig для своей системы
    3. пытаться что то гуглить, заодно описать свои достижения тут   

Запук
    
    commands.png схема переходов состояний application server
    scheme.png схема взаимодействия компонентов системы
    README.md в папках (scr/recognition_server, scr/voice_record, scr/application_server)
    
recognition_server: 
    
    преобразует звук в текст

voice_record: 

    запись звука и его отправка на application server
    
    record_and_send_to_recognize_server.py 
    просто тестирование команд, не меняет состояние application server
    
    record_and_send_to_application_server.py
    меняет состояние application server, скрипт для нормально работы
    
    nenavijy_python_i_windy_pyst_oni_sdohnyt_ap.py
    тоже что и record_and_send_to_application_server.py, только посылается текстовая команда а не голос
    
    nenavijy_python_i_windy_pyst_oni_sdohnyt_rs.py
    тоже что и record_and_send_to_recognize_server.py , только посылается текстовая команда а не голос

application_server:  

    принимает звук от voice record, 
    отправляет его на recognition server, 
    обрабатывает его ответ для изменений на фронте
    
Хрень из блокнота:

    Дата 
    сейчас
    
    Диагноз
    чтото
    
    жалобы
    <резкая|ноющая> <боль|сыпь> <часть тела>
    
    Данные лаб
    анализ крови <в норме|повышенные<АСТ|билирубин|глюкоза|лейкоциты>>
    повышенная подвижность <часть тела>
    
    лек препараты (окошко)
    
    назначения
    <наблюдение|консультация> <врач>
    
    Листок
    нетрудоспособен в течении <число> <дней|недель|месяцев>
    ограничить подвижность [<часть тела>] на <число> <дней|недель|месяцев>
    
    льготные пофигу
    
    врач 
    ФИО
    
    {
    "user": "Никулин Н.А.",
    "doctor_id": 0, 
    "doctors": [{"id": 0, "name": "Терапевт"}],
    "selected_visit": 0, 
    "selected_visit_info":{
        "date": "21/11/2018",
        "complaints":["Резкая сыпь во внутреннем ухе"],
        "dynamic":["повышенная подвижность верхней челюсти"],
        "destinations":["наблюдение психиатр"],
        "tablets":["аспирин три раза в день в течении месяца"],
        "disability":["нетрудоспособен в течении трех дней"],
        "priviliges": [""],
        "user": "Никулин Н.А.",
    },
    "visits": [{"id": 0, "date": "21/11/2018", "doctor": "Никулин Н.А."}],
    "visit_info":{
        "date": "21/11/2018",
        "complaints":["Резкая сыпь во внутреннем ухе"],
        "dynamic":["повышенная подвижность верхней челюсти"],
        "destinations":["наблюдение психиатр"],
        "tablets":["аспирин три раза в день в течении месяца"],
        "disability":["нетрудоспособен в течении трех дней"],
        "priviliges": [""],
        "user": "Никулин Н.А.",
    },
    "tablet_editor": {"name": null, "dose": null, "duration": null},
    "patient_info": ["Фамилия: Деревянко", "Имя: Артём", "Отчество: Алексеевич", "Противопоказания, "Аспирин"], 
    }


 
        
      