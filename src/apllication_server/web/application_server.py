from flask import Flask
from src.apllication_server.src.controller.alisa_dialogs_controller import alisa_api
from src.apllication_server.src.controller.root_controller import hello_api
from src.apllication_server.src.controller.front_controller import card_api
from src.apllication_server.src.controller.state_controller import sphinx_api

from src.apllication_server.src.model.hospital_utils import Hospital, Patient, Doctor, Visit, Tablet, Test

# pip install flask_cors
from flask_cors import CORS

import random

app = Flask(__name__)
CORS(app)

app.register_blueprint(alisa_api)
app.register_blueprint(hello_api)
app.register_blueprint(sphinx_api)
app.register_blueprint(card_api)  # albatros


def get_random(collection):
    return collection[random.randrange(len(collection))]


doctor = ['Никулин Н.А.', 'Соболев Н.И.', 'Медко К.К.', 'Куликов А.М.', 'Березкин К.Е.']



test_names = ['кровь', 'рентген', 'ЭКГ', 'МРТ']

patient_names = ['Михаил', 'Артём', 'Виктор']
patient_lnames = ['Иванов', 'Деревянко', 'Любомудров']
patient_pnames = ['Николаевич', 'Алексеевич', 'Степанович']

test_statuses = ['Еще не знаем', 'Все хорошо', 'Все плохо']
illnesses = ['Волчанка', 'Волчанка', 'Волчанка', 'Волчанка', 'Мигрень', 'Голод']

doctor_names = ['Терапевт', 'Хирург', 'Отоларинголог', 'Окулист', 'Невролог']

visits_src_1 = ("Первичный прием", "Повторный прием", "Профилактический прием")
visits_src_2 = ("Болит все", "Болит местами", "Не болит и это беспокоит")

complaints = ["сыпь на голове", "ноющая боль в ноге", "резкая боль в шее"]
dynamic = ["кровь в норме", "повышенная подвижность колена", "повышенные лейкоциты"]
destinations = ["консультация хирурга", "наблюдение психолога", "консультация отоаларинголога"]
tablet_names = ["анальгин", "ринза", "ибупрафен"]
disability = ["ограничить подвижность головы", "нетрудоспособность два дня", "ограничить подвижность шеи"]

dates = [x + 1 for x in range(30)]

patients = []
for i in range(3):
    tablets = [Tablet(get_random(tablet_names)) for j in range(2)]
    tests = [Test(get_random(test_names), '{}/11/2018'.format(get_random(dates)),
                  '{}/12/2018'.format(get_random(dates)) if j > 0 else 'Не проведен',
                  test_statuses[j], j) for j in range(3)]
    doctors = []
    for k, name in enumerate(doctor_names):
        visits = []
        for j in range(3):
            date = '{}/11/2018'.format(get_random(dates))
            story = {
                "date": date,
                "complaints": [get_random(complaints)],
                "dynamic": [get_random(dynamic)],
                "destinations": [get_random(destinations)],
                "tablets": [get_random(tablet_names)],
                "disability": [get_random(disability)],
                "privileges": [""],
                "user": doctor[k],
            }
            visits.append(Visit(story, date,
                                "", get_random(illnesses)))
        doctors.append(Doctor(name, visits))
    patients.append(Patient(patient_lnames[i], patient_names[i], patient_pnames[i],
                            tablets=tablets, analisis=tests, doctors=doctors))

    Hospital.patients = patients

if __name__ == "__main__":
    app.run(port=5002)
