$(document).ready(function () {
var inputs = document.getElementsByTagName("input");
for(let i=0; i<inputs.length; i++){
  inputs[i].addEventListener("click", clickoninput, {passive: true} );
};
});

function clickoninput(e ){
   let target = $( e.target );
   target.focus();
};

//----------------- Jquery help function ----------
$.fn.isValid = function () {
  return this[0].checkValidity();
};

$("#login-form")
  .form({
    fields: {
      email: {
        identifier  : 'email',
        rules: [
          {
            type   : 'email',
            prompt : 'Please enter a valid e-mail'
          }
        ]
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a password'
          },
          {
            type   : 'minLength[6]',
            prompt : 'Your password must be at least {ruleValue} characters'
          }
        ]
      }
    }
  });

$("#reg-form")
  .form({
    fields: {
      email: {
        identifier  : 'email',
        rules: [
          {
            type   : 'email',
            prompt : 'Please enter a valid e-mail'
          }
        ]
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a password'
          },
          {
            type   : 'minLength[6]',
            prompt : 'Your password must be at least {ruleValue} characters'
          }        
        ]
      },
      password_repeat: {
        identifier  : 'password_repeat',
        rules: [
          {
            type   : 'match[password]',
            prompt : 'Please put the same value in both password fields'
          }
        ]
      },
      organisation: {
        name  : 'organisation',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter an organisation'
          }
        ]
      },
      empty: {
        identifier  : 'user_md',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a specialty'
          }
        ]
      }
    }
  });

 var goToLk = function(e){
    e.preventDefault();
    if($("#login-form").isValid()){
      $(location).attr("href", "main.html");
    }
  };

  // $('.message .close')
  // .on('click', function() {
  //   $(this)
  //     .closest('.message')
  //     .transition('fade')
  //   ;
  // });

  function closeMessage(e){
    let target = $( e.target );
    console.log(target);
    target.closest('.message').closest('.item').remove();
  };

function pushAlert(message, type) {
  let newEl = $("<li class='item'><div class='ui large " + type +  " message'><i class='close icon' onclick='closeMessage(event)'></i><p>" + message + "</p></div></li>");
  $("#alertList").append(newEl);
};
