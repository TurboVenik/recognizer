var serverPath = "http://localhost:5002";
 

//----------------- Jquery help function ----------
$.fn.isValid = function () {
  return this[0].checkValidity();
};

/**
 * ----------- Knockout View Model ------------
 */
var doctor = function (data) {
  var self = this;
  self.id = ko.observable(data.id);  
  self.name = ko.observable(data.name);  
  self.active = ko.observable(false);  
};

var visit = function (data) {
  var self = this;
  self.id = ko.observable(data.id);  
  self.date = ko.observable(data.date);
  self.info = ko.observable(data.info); 
  self.doc = ko.observable(data.doc); 
  self.active = ko.observable(false);  
};
var analysis = function (data) {
  var self = this;
  self.id = ko.observable(data.id);
  self.date = ko.observable(data.date);
  self.text = ko.observable(data.text);
  self.state = ko.observable(data.state);
};
var medicine = function (data) {
  var self = this;
  self.name = ko.observable(data.name);
  self.dose = ko.observable(data.dose);
  self.duration = ko.observable(data.duration);
};

//----------- View Model --------------------------
function ViewModel() {
  var self = this;
    self.user = ko.observable(null),
  	self.selectedDoc = ko.observable(null),
  	self.selectedVisit = ko.observable(null),
    self.doctors = ko.observableArray([]),
    self.visits = ko.observableArray([]),
    self.info1 = ko.observableArray([]),
    self.info2 = ko.observableArray([]),
    self.analysis = ko.observableArray([]),
    self.tablet_editor = ko.observable(null),
    // ---------- functions ------------------

  self.parseString = function (str) {
  	console.log(str);
  	let arr = str.split(":",2);
  	var str ="<strong>"+arr[0]+": </strong>"+" "+arr[1];
  	return str;
  },
    self.setStatusClass = function (status) {
      switch (status) {
        case 0:
          return " active ";
        case 1:
          return " positive ";
				case 2:
          return " negative ";          
        default:
          return " ";
      }
    }

};
/**
 * [Model description]
 * @type {ViewModel}
 */
var Model = new ViewModel();
ko.applyBindings(Model);

//----------- Subscribes --------------------------
Model.selectedDoc.subscribe(function (item) {
 //  if (item[0].index == 0 && item[0].status == "added") {
	// }; 
});
Model.tablet_editor.subscribe(function (item) {
    if(Model.tablet_editor()){
        if(!$('#tablet-editor-modal').modal('is active')){
            $('#tablet-editor-modal').modal('show');
        }
    } else {
        if($('#tablet-editor-modal').modal('is active')){
            $('#tablet-editor-modal').modal('hide');
        }
    }
});
//-------------------------------------
$(document).ready(function () {
  var getInfpTimerId = setInterval(getInfo, 1000);
  // getInfo();
});

function getInfo() {
 
    $.ajax({
      xhrFields: {
      	  // withCredentials: true
      },
      headers: { },
      contentType: "application/json",
      method: "GET",
      url: serverPath + "/top/card",
      success: function (data, textStatus, xhr) {
        // $.each(data.pos, function () {
        //   Model.terminals.push(new pos(this));
        // });
        console.log(data);
        var response = JSON.parse(data);
        // var response =  tmpdata;
        Model.user(response.user);
        
        Model.doctors.removeAll(); 
				$.each(response.doctors, function () {
          Model.doctors.push(new doctor(this));
        });	        
				// $.each(response.doctors, function () {
				// 	console.log("doctor", this);
    //      let res = Model.doctors().findIndex(x => x.id() == this.id);  
    //       if(res == -1) {
    //       	Model.doctors.push(new doctor(this));
    //       } else {
    //       	if(Model.doctors()[res].name() != this.name){
    //       		Model.doctors().remove(Model.doctors()[res]);
    //       		Model.doctors.push(new doctor(this));
    //       	};
    //       };
    //     });	

  	  	console.log(response.visits);
				Model.visits.removeAll(); 
				$.each(response.visits, function () {
          Model.visits.push(new visit(this));
        });	    	  	
  	  // 	$.each(response.visits, function () {
					// console.log("visit:",this);
     //     let res = Model.visits().findIndex(x => x.id() == this.id);  
     //      if(res === -1) {
     //      	console.log("visit ", this);
     //      	Model.visits.push(new visit(this));
     //      } else {
     //      	if(Model.visits()[res].info() != this.info){
     //      		Model.visits().remove(Model.visits()[res]);
     //      		Model.visits.push(new visit(this));
     //      	};
     //      };
     //    });	
           console.log("docs:",Model.doctors());
        console.log("response:",response.doctor_id);
        console.log("selectedDoc:",Model.selectedDoc());
        if(Model.selectedDoc() && response.doctor_id != null){
          console.log("1");
          if(Model.selectedDoc() != response.doctor_id){
            console.log("2");
            Model.doctors().find(x => x.id() === Model.selectedDoc()).active(false);
            Model.doctors().find(x => x.id() === response.doctor_id).active(true);
            Model.selectedDoc(response.doctor_id);            
          }else {
            console.log("2/2");
            Model.doctors().find(x => x.id() === response.doctor_id).active(true);
          }
        }else {
          console.log("3");
          if(!Model.selectedDoc() && response.doctor_id != null){
            console.log("4");
            Model.doctors().find(x => x.id() === response.doctor_id).active(true);
            Model.selectedDoc(response.doctor_id);                
          }
           if(Model.selectedDoc() && response.doctor_id == null){
            console.log("5");
            Model.doctors().find(x => x.id() === Model.selectedDoc()).active(false);
            Model.selectedDoc(null);
           }
          if(!Model.selectedDoc() && response.doctor_id == null){
            console.log("6");
            // Model.doctors().find(x => x.id() === Model.selectedDoc()).active(false);
            // Model.selectedDoc(null);
           }           
        };


				// if(Model.selectedDoc() && Model.selectedDoc() != response.doctor_id){
				// 	console.log("!!!! 1");
				// 	Model.doctors().find(x => x.id() === Model.selectedDoc()).active(false);
				// 	Model.doctors().find(x => x.id() === response.doctor_id).active(true);
				// 	Model.selectedDoc(response.doctor_id);
				// } else {
				// 	if(!Model.selectedDoc()){
				// 		console.log("!!!! 2")
				// 		Model.doctors().find(x => x.id() === response.doctor_id).active(true);
				// 		Model.selectedDoc(response.doctor_id);						
				// 	} else {
				// 		Model.doctors().find(x => x.id() === response.doctor_id).active(true);
				// 	}
				// 	;
				// };

				if(Model.selectedVisit() && response.selected_visit){
					if(Model.selectedVisit() != response.selected_visit){
						Model.visits().find(x => x.id() === Model.selectedVisit()).active(false);
						Model.visits().find(x => x.id() === response.selected_visit).active(true);
						Model.selectedVisit(response.selected_visit);
					} else {
						Model.visits().find(x => x.id() === response.selected_visit).active(true);
					};						
				} else {
					// if()
				};
 
				 Model.info1.removeAll(); 
				$.each(response.info1, function (index) {
					console.log(this);
          Model.info1.push(this);
        }); 

 				Model.info2.removeAll(); 
				$.each(response.info2, function (index) {
					console.log(this);
          Model.info2.push(this);
        });     

 				Model.analysis.removeAll();
				$.each(response.analis, function (index) {
					console.log(this);
          Model.analysis.push(new analysis(this));
        });         
				// $.each(response.info2, function () {
    //      let res = Model.info2().findIndex(this);  
    //       if(res == -1) {
    //       	Model.info2.push(this);
    //       } else {
    //       	if(Model.info2()[res] != this){
    //       		Model.info2().remove(Model.info2()[res]);
    //       		Model.info2.push(this);
    //       	};
    //       };
    //     });         
				// $.each(response.analis, function () {
    //      let res = Model.analysis().findIndex(this);
    //       if(res == -1) {
    //       	Model.analysis.push(this);
    //       } else {
    //       	if(Model.analysis()[res] != this){
    //       		Model.analysis().remove(Model.analysis()[res]);
    //       		Model.analysis.push(this);
    //       	};
    //       };
    //     });

        console.log(response.tablet_editor);
//        if(Model.tablet_editor())? Model.tablet_editor(null) : ;
        (response.tablet_editor) ? Model.tablet_editor(new medicine(response.tablet_editor)) : Model.tablet_editor(null) ;
        console.log("Tablet-editor: ",Model.tablet_editor());
      },
      error: function (data) {
        try {
          pushAlert("Ошибка: " + data.status + " " + data.error_description, "negative");
        } catch (error) {
          pushAlert("Ошибка при обновлении информации", "negative");
        };
      }
    }); 
};


function closeMessage(e){
	let target = $( e.target );
	console.log(target);
	target.closest('.message').closest('.item').remove();
 };

function pushAlert(message, type) {
  let newEl = $("<li class='item'><div class='ui large " + type +  " message'><i class='close icon' onclick='closeMessage(event)'></i><p>" + message + "</p></div></li>");
  $("#alertList").append(newEl);
};

var tmpdata = new Object({
    "user": "Иванов Петр Сергеич",
    "doctor_id": 0,
    "selected_visit": 0,
    "analis": [
        {
            "id": 0,
            "text": "Анализ 1:Все хорошо. Назначены: 27/1/2018. Актуальны до: 12/12/2018",
            "state": 1
        },
        {
            "id": 1,
            "text": "Анализ 2:Все плохо. Назначены: 27/5/2018. Актуальны до: 12/12/2018",
            "state": 2
        },
        {
            "id": 2,
            "text": "Анализ 3:Еще не знаем. Назначены: 27/11/2018. Актуальны до: Не проведен",
            "state": 0
        }
    ],
    "doctors": [
        {
            "id": 0,
            "name": "Иванов Петр Сергеич"
        },
        {
            "id": 1,
            "name": "Попов Огурец"
        }
    ],
    "visits": [
        {
            "id": 0,
            "info": "Первичный прием",
            "data": "26/11/2018 17.33.42"
        }
    ],
    "info1": [
        "Фамилия: Иван",
        "Имя: Иванов",
        "Отчество: Иванович",
        "Принимаемые лекарства: Анальгин c 20/1/2018 по 27/1/2018, Ибупрофен c 20/2/2018 по 27/5/2018, Шарюзаутюг c 20/4/2018 по 10/7/2018"
    ],
    "info2": [
        "0 Причина приема: Первичный прием",
        "1 Жалобы: болит мозг",
        "2 Выписанные лекарства: Анальгин",
        "3 Диагноз: Хакатонщик"
    ]
}

);
