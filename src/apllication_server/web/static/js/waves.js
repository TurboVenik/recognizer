var camera;
var scene;
var renderer;
var controls;
var container;
var points;
var pointsMaterial;

const rows = [];
let pointsGeometry;

init();
animate();

function init() {
  container = document.getElementById( 'waves' );
  camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 100  );
  camera.position.z = 100;
  scene = new THREE.Scene();

  renderer = new THREE.WebGLRenderer();
  renderer.setClearColor(new THREE.Color(0x21ba45));
  renderer.setSize( window.innerWidth, window.innerHeight );
  container.appendChild( renderer.domElement );
  window.addEventListener( 'resize', onWindowResize, false );
  
  controls = new THREE.OrbitControls( camera );
  
  noise.seed(0);
  
  pointsGeometry = new THREE.Geometry();
  
  const fieldSize = 50;
  for( let y=0; y<fieldSize; y++ ){
    const columns = [];
    rows.push( columns );
    for( let x=0; x<fieldSize; x++ ){
      const v = new THREE.Vector3( x * 2 - fieldSize, 0, y * 2 - fieldSize);
      const color = new THREE.Color(0,0,0);      
      pointsGeometry.colors.push( color );
      pointsGeometry.vertices.push( v );
      columns.push( v );
    }
  }
  
  pointsMaterial = new THREE.PointsMaterial({
    size: 0.5,
    // vertexColors: THREE.VertexColors
    color: 0xffffff,
    // vertexColors: THREE.VertexColors
  })
  
 // pointsMaterial.vertexColors = true;
  
  points = new THREE.Points( pointsGeometry, pointsMaterial );
  
  scene.add( points );
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
  requestAnimationFrame( animate );
  controls.update();
  render(); 
}

function render() {
  let index = 0;
  for(let y=0; y<rows.length; y++){
    const column = rows[y];
    for(let x=0; x<column.length; x++){
      
      const move = noise.simplex3(x * 0.01, y * 0.04, Date.now() * 0.0002);
      const move2 = noise.simplex3(x * 0.1, y * 0.1, Date.now() * 0.0004);
      
      const v = column[x];

      v.y = (move) * 10 + move2 * 7;
      
      const color = pointsGeometry.colors[index];
      // color.r = (v.y+3) / 10 ;
      // color.g = (v.y) / 16;
      // color.b = (move2 + 1)/2;
      color.r = (v.y+3) / 10 ; // 64
      color.g = (v.y) / 16; //196
      color.b = (move2 + 1)/2;   //101
      
      index++;
    }
            
  }
  
  pointsGeometry.verticesNeedUpdate = true;
  pointsGeometry.colorsNeedUpdate = true;
  
  renderer.render( scene, camera );
}


