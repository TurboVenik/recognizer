"use strict";

var serverPath = "http://127.0.0.1:5002";
/**
 * ----------- Knockout View Model ------------
 */

var doctor = function doctor(data) {
  var self = this;
  self.id = ko.observable(data.id);
  self.name = ko.observable(data.name);
  self.active = ko.observable(false);
};

var visit = function visit(data) {
  var self = this;
  self.id = ko.observable(data.id);
  self.date = ko.observable(data.date);
  self.info = ko.observable(data.info);
  self.doctor = ko.observable(data.doctor);
  self.active = ko.observable(false);
  self.incrementedId = ko.computed(function() {
        return self.id() + 1;
    });
};

var visitInfo = function visitInfo (data) {
  //console.log("visitInfo constructor");
  var self = this;
  if(data){
    self.date = ko.observable(data.date || "");

    self.complaints = ko.computed(function () {
      var all = ko.observableArray([]);
      $.each(data.complaints, function () {
        all.push(this);
      });
      return all();
    });
    self.dynamic = ko.computed(function () {
      var all = ko.observableArray([]);
      $.each(data.dynamic, function () {
        all.push(this);
      });
      return all();
    });
    self.destinations = ko.computed(function () {
      var all = ko.observableArray([]);
      $.each(data.destinations, function () {
        all.push(this);
      });
      return all();
    });
    self.tablets = ko.computed(function () {
      var all = ko.observableArray([]);
      $.each(data.tablets, function () {
        all.push(this);
      });
      return all();
    });
    self.disability = ko.computed(function () {
      var all = ko.observableArray([]);
      $.each(data.disability, function () {
        all.push(this);
      });
      return all();
    });
    self.priviliges = ko.computed(function () {
      var all = ko.observableArray([]);
      $.each(data.priviliges, function () {
        all.push(this);
      });
      return all();
    });
    self.user = ko.observable(data.user || "");
  } else {
    self.date = ko.observable("");
    self.complaints = ko.observableArray([]);
    self.dynamic = ko.observableArray([]);
    self.destinations = ko.observableArray([]);
    self.tablets = ko.observableArray([]);
    self.disability = ko.observableArray([]);
    self.priviliges = ko.observableArray([]);
    self.user = ko.observable("");
  }

 // console.log(self);
};

var medicine = function medicine(data) {
  var self = this;
  self.name = ko.observable(data.name);
  self.dose = ko.observable(data.dose);
  self.duration = ko.observable(data.duration);
}; //----------- View Model --------------------------

function ViewModel() {
  var self = this;
  self.user = ko.observable(null),
    self.selected_doc = ko.observable(null),
    self.selected_visit = ko.observable(null),
    self.doctors = ko.observableArray([]),
    self.visits = ko.observableArray([]),
    self.patient_info = ko.observableArray([]),
    self.current_visit_info = ko.observable(null),
    self.tablet_editor = ko.observable(null),
    self.selected_visit_info = ko.observable(null),

    // ---------- functions ------------------
    self.parseString = function(str) {
      //console.log(str);
      var arr = str.split(":", 2);
      var str = "<strong>" + arr[0] + ": </strong>" + " " + arr[1];
      return str;
    },

    self.setStatusClass = function(status) {
      switch (status) {
        case 0:
          return " active ";

        case 1:
          return " positive ";

        case 2:
          return " negative ";

        default:
          return " ";
      }
    };
}

/**
 * [Model description]
 * @type {ViewModel}
 */

var Model = new ViewModel();
ko.applyBindings(Model);

//----------- Subscribes --------------------------

Model.selected_doc.subscribe(function(item) {});
Model.tablet_editor.subscribe(function(item) {
  if (Model.tablet_editor()) {
    if (!$("#tablet-editor-modal").modal("is active")) {
      $("#tablet-editor-modal").modal("show");
    }
  } else {
    if ($("#tablet-editor-modal").modal("is active")) {
      $("#tablet-editor-modal").modal("hide");
    }
  }
});
Model.selected_visit_info.subscribe(function(item) {
  if (Model.selected_visit_info()) {
    if (!$("#selected-visit-modal").modal("is active")) {
      $("#selected-visit-modal").modal("show");
    }
  } else {
    if ($("#selected-visit-modal").modal("is active")) {
      $("#selected-visit-modal").modal("hide");
    }
  }
});
//-------------------------------------

$(document).ready(function() {
  var getInfpTimerId = setInterval(getInfo, 1000);
// getInfo();
});


function getInfo() {
  $.ajax({
    xhrFields: {
      // withCredentials: true
    },
    headers: {},
    contentType: "application/json",
    method: "GET",
    url: serverPath + "/top/card",
    success: function success(data, textStatus, xhr) {
      //console.log(data);
      var response = JSON.parse(data);
      console.log("New data: ", response);
      Model.user(response.user);
      Model.doctors.removeAll();
      $.each(response.doctors, function() {
        Model.doctors.push(new doctor(this));
      });
      //console.log(response.visits);
      Model.visits.removeAll();
      $.each(response.visits, function() {
        Model.visits.push(new visit(this));
      });
      //console.log("docs:", Model.doctors());
      //console.log("response:", response.doctor_id);
      //console.log("selected_doc:", Model.selected_doc());

      if (Model.selected_doc() && response.doctor_id != null) {
        //console.log("1");

        if (Model.selected_doc() != response.doctor_id) {
          //console.log("2");
          Model.doctors()
            .find(function(x) {
              return x.id() === Model.selected_doc();
            })
            .active(false);
          Model.doctors()
            .find(function(x) {
              return x.id() === response.doctor_id;
            })
            .active(true);
          Model.selected_doc(response.doctor_id);
        } else {
          //console.log("2/2");
          Model.doctors()
            .find(function(x) {
              return x.id() === response.doctor_id;
            })
            .active(true);
        }
      } else {
        //console.log("3");

        if (!Model.selected_doc() && response.doctor_id != null) {
          //console.log("4");
          Model.doctors()
            .find(function(x) {
              return x.id() === response.doctor_id;
            })
            .active(true);
          Model.selected_doc(response.doctor_id);
        }

        if (Model.selected_doc() && response.doctor_id == null) {
          //console.log("5");
          var doc = Model.doctors()
            .find(function(x) {
              return x.id() === Model.selected_doc();
            });
          console.log(doc);
          if(doc){doc.active(false);}

          Model.selected_doc(null);
        }

        if (!Model.selected_doc() && response.doctor_id == null) {
          //console.log("6"); // Model.doctors().find(x => x.id() === Model.selected_doc()).active(false);
          // Model.selected_doc(null);
        }
      }

      if (Model.selected_visit() && response.selected_visit != null) {
        if (Model.selected_visit() != response.selected_visit) {
          Model.visits()
            .find(function(x) {
              return x.id() === Model.selected_visit();
            })
            .active(false);
          Model.visits()
            .find(function(x) {
              return x.id() === response.selected_visit;
            })
            .active(true);
          Model.selected_visit(response.selected_visit);
        }
      } else {
        if (!Model.selected_visit() && response.selected_visit  != null) {
          Model.visits()
              .find(function (x) {
                return x.id() === response.selected_visit;
              })
              .active(true);
          Model.selected_visit(response.selected_visit);
        }

        if (Model.selected_visit() && response.selected_visit == null) {
          Model.visits()
            .find(function(x) {
              return x.id() === Model.selected_visit();
            })
            .active(false);
          Model.selected_visit(null);
        }

      };

      Model.patient_info.removeAll();
      $.each(response.patient_info, function(index) {
        //console.log(this);
        Model.patient_info.push(this);
      });
      // Model.current_visit_info.removeAll();
      // $.each(response.visit_info, function(index) {
      //   //console.log(this);
      //   Model.current_visit_info.push(this);
      // });
      response.visit_info
        ? Model.current_visit_info(new visitInfo(response.visit_info))
        : Model.current_visit_info(new visitInfo(null));
      //console.log("current_visit_info: ", Model.current_visit_info());

      // console.log("tablet_editor", response.tablet_editor);
      response.tablet_editor
        ? Model.tablet_editor(new medicine(response.tablet_editor))
        : Model.tablet_editor(null);
      //console.log("Tablet-editor: ", Model.tablet_editor());

      //console.log(response.selected_visit_info);
      response.selected_visit_info
        ? Model.selected_visit_info(new visitInfo(response.selected_visit_info))
        : Model.selected_visit_info(null);
      //console.log("selected_visit_info: ", Model.selected_visit_info());
    },
    error: function error(data) {
      try {
        pushAlert(
          "Ошибка: " + data.status + " " + data.error_description,
          "negative"
        );
      } catch (error) {
        pushAlert("Ошибка при обновлении информации", "negative");
      }
    }
  });
}

function closeMessage(e) {
  var target = $(e.target);
  //console.log(target);
  target
    .closest(".message")
    .closest(".item")
    .remove();
}

function pushAlert(message, type) {
  var newEl = $(
    "<li class='item'><div class='ui large " +
      type +
      " message'><i class='close icon' onclick='closeMessage(event)'></i><p>" +
      message +
      "</p></div></li>"
  );
  $("#alertList").append(newEl);
}
