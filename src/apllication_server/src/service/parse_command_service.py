import datetime

from src.apllication_server.src.enum.enums import STATE
from src.apllication_server.src.model.hospital_utils import Hospital
from random import randint


class CommandParser:
    test_names = ['кровь',
                  'флюорография']

    command_map = {
        'начать прием': ['начать прием',
                         'начнем прием',
                         'добрый день',
                         'здравствуйте'],

        'закончить прием': ['закончить прием',
                            'завершить прием',
                            'закончить'],

        'добавить лекарство': ['добавить лекарство',
                               'прописать лекарство',
                               'добавить препорат',
                               'прописать препорат'],

        'добавить анализ': ['добавить анализ'],

        'пациент': ['пациент'],

        'добавить диагноз': ['добавить диагноз',
                             'прописать диагноз'],

        'добавить заметку': ['добавить заметку',
                             'добавить запись'],

        'выбрать врача': ['выбрать врача',
                          'показать врача',
                          'покажи врача',
                          'посмотреть врача'],

        'выбрать прием': ['выбрать прием номер', 'показать прием номер',
                          'покажи прием номер', 'посмотреть врача',
                          'выбрать прием', 'показать прием',
                          'покажи прием', 'посмотреть']

    }

    suggestion_map = {
        STATE.PATIENT_NOT_SELECTED: ['Пациент Иванов Иван Иванович'],

        STATE.PATIENT_SELECTED: ['Начать прием',
                                 'Пациент Иванов Иван Иванович'],

        STATE.VISIT_STARTED: ['Добавить лекарство анальгин',
                              'Добавить анализ кровь',
                              'Закончить прием',
                              'Добавить диагноз грипп',
                              'Добавить заметку минус пациент',
                              'выбрать прием номер 1',
                              'выбрать врача хирург']
    }

    answer_map = {
        'начать прием': ['Прием начат',
                         'Начало нового приема',
                         'Ну что, приступим'],

        'закончить прием': ['Прием закончен',
                            'Завершение приема',
                            'Закончим на этом'],

        'добавить лекарство': ['Добавлено лекарство {}',
                               'Препорат {} добавлен в перечень',
                               'Препорат {} прописан пациенту',
                               'Пациенту спешно добавлен препорат {}'],

        'добавить анализ': ['Добавлен анализ {}',
                            'Анализ {} добавлен'],

        'пациент': ['Пациент {} {} {} найден',
                    'Информация о пациенте найдена'],

        'добавить диагноз': ['Добавлен диагноз {}',
                             'Диагноз {} добавлен'],

        'добавить заметку': ['Добавлена заметка {}',
                             'Новая заметка {}'],

        'выбрать врача': ['Выбран врач {}'],

        'выбрать прием': ['Выбран прием номер {}']
    }

    error_text = ['Посмотри в подсказки',
                  'Неизвестная команда',
                  'Придумай что нибудь нормальное']

    error_map = {

    }

    @staticmethod
    def return_random_from(mas):
        return mas[randint(0, mas.__len__() - 1)]

    @staticmethod
    def find_command_in_mas(tokens, commands, stat):
        commands = CommandParser.command_map[commands]
        for command in commands:
            command_split = command.split(' ')
            if command_split.__len__() > tokens.__len__():
                continue
            f = True
            for i in range(command_split.__len__()):
                if command_split[i] != tokens[i]:
                    f = False
                    break
            if f:
                stat['length'] = command.__len__()
                stat['count_of_words'] = command_split.__len__()
                return True
        return False

    @staticmethod
    def visit_started(req, res, session_storage, user_id):

        return_text = CommandParser.return_random_from(CommandParser.error_text)

        stat = {}

        if CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'закончить прием', stat):
            if Hospital.end_visit():
                return_text = CommandParser.return_random_from(CommandParser.answer_map['закончить прием'])
                session_storage[user_id]['state'] = STATE.PATIENT_NOT_SELECTED
                session_storage[user_id]['suggests'] = CommandParser.suggestion_map[STATE.PATIENT_NOT_SELECTED]
            else:
                return_text = 'Ошибка в окончании приема(false)'

        elif CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'добавить лекарство', stat):
            start_date = None
            end_date = None

            end_of_date = -1
            start_of_date = -1

            for entity in req['request']['nlu']['entities']:
                if entity['type'] == 'YANDEX.DATETIME':
                    day = entity["value"]["day"]
                    month = entity["value"]["month"]

                    end_of_date = entity["tokens"]["end"] if end_of_date == -1 \
                                                             or (end_of_date < entity["tokens"]["end"] \
                                                                 and end_of_date != -1) else end_of_date

                    start_of_date = entity["tokens"]["start"] if start_of_date == -1 \
                                                                 or (start_of_date > entity["tokens"]["end"] \
                                                                     and start_of_date != -1) else start_of_date

                    if start_date is None:
                        start_date = datetime.date(2018, int(month), int(day))
                    else:
                        end_date = datetime.date(2018, int(month), int(day))

            if start_date is None and end_date is not None or end_date is None and start_date is not None:

                start_of_date = start_of_date - 1

            elif start_date is None or end_date is None:
                start_of_date = req['request']['nlu']['tokens'].__len__()

            tablet_name_tokens = req["request"]["nlu"]["tokens"][stat['count_of_words']: start_of_date]

            tablet_name = ''

            for item in tablet_name_tokens:
                tablet_name += item + ' '

            tablet_name = tablet_name.strip()

            if Hospital.add_tablet(tablet_name, start_date, end_date):
                return_text = CommandParser.return_random_from(
                    CommandParser.answer_map['добавить лекарство']) \
                    .format(tablet_name, str(start_date), str(end_date))
            else:
                return_text = 'лекарство не может быть добавлено "' + tablet_name + '"(false)'

        elif CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'добавить анализ', stat):
            test_name = req['request']['command'][stat['length'] + 1: req['request']['command'].__len__()]

            if Hospital.add_test(test_name, None, None):
                return_text = CommandParser.return_random_from(CommandParser.answer_map['добавить анализ']) \
                    .format('"' + test_name + '"')
            else:
                return_text = 'Анализ не может быть добавлен "' + test_name + '"(false)'

        elif CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'добавить диагноз', stat):
            diagnosis_name = req['request']['command'][stat['length'] + 1: req['request']['command'].__len__()]

            if Hospital.add_diagnosis(diagnosis_name):
                return_text = CommandParser.return_random_from(CommandParser.answer_map['добавить диагноз']) \
                    .format('"' + diagnosis_name + '"')
            else:
                return_text = 'Диагноз не может быть довбавлен "' + diagnosis_name + '"(false)'

        elif CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'добавить заметку', stat):
            note_text = req['request']['command'][stat['length'] + 1: req['request']['command'].__len__()]

            if Hospital.add_note(note_text):
                return_text = CommandParser.return_random_from(CommandParser.answer_map['добавить заметку']) \
                    .format('"' + note_text + '"')
            else:
                return_text = 'Заметка не может быть добавлена "' + note_text + '"(false)'
        elif CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'выбрать врача', stat):
            doctor_type = req['request']['command'][stat['length'] + 1: req['request']['command'].__len__()]
            if Hospital.change_selected_doctor(doctor_type):
                return_text = CommandParser.return_random_from(CommandParser.answer_map['выбрать врача']) \
                    .format('"' + doctor_type + '"')
            else:
                return_text = 'Врач не может быть выбран "' + doctor_type + '"(false)'
        elif CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'выбрать прием', stat):
            visit_id = req['request']['nlu']['tokens'][stat['count_of_words']: req['request']['nlu']['tokens'].__len__()][0]
            try:
                visit_id = int(visit_id)
                if Hospital.start_watching_visit(visit_id):
                    return_text = CommandParser.return_random_from(CommandParser.answer_map['выбрать прием']) \
                        .format('"' + str(visit_id) + '"')
                else:
                    return_text = 'Приём не может быть выбран "' + str(visit_id) + '"(false)'
            except:
                return_text = 'Номер посещения должен быть числом'
        res['response']['text'] = return_text
        res['response']['buttons'] = CommandParser.suggestions(session_storage[user_id])

    @staticmethod
    def patient_selected(req, res, session_storage, user_id):
        return_text = CommandParser.return_random_from(CommandParser.error_text)

        stat = {}
        if CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'начать прием', stat):
            if Hospital.start_visit():
                return_text = CommandParser.return_random_from(CommandParser.answer_map['начать прием'])
                session_storage[user_id]['state'] = STATE.VISIT_STARTED
                session_storage[user_id]['suggests'] = CommandParser.suggestion_map[STATE.VISIT_STARTED]
            else:
                return_text = 'Не возможно начать посещение(false)'

        elif CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'пациент', stat):
            for entity in req['request']['nlu']['entities']:
                if entity['type'] == 'YANDEX.FIO':
                    try:
                        last_name = entity["value"]["last_name"]
                        first_name = entity["value"]["first_name"]
                        patronymic_name = entity["value"]["patronymic_name"]

                        if Hospital.change_current_patinent(first_name, last_name, patronymic_name):
                            return_text = CommandParser.return_random_from(CommandParser.answer_map['пациент']) \
                                .format(last_name.title(), first_name.title(), patronymic_name.title())

                            session_storage[user_id]['state'] = STATE.PATIENT_SELECTED
                            session_storage[user_id]['suggests'] = CommandParser.suggestion_map[STATE.PATIENT_SELECTED]
                        else:
                            return_text = 'Необходимо указать полное имя (false)'

                        break
                    except Exception as e:
                        return_text = 'Необходимо указать полное имя'

        res['response']['text'] = return_text
        res['response']['buttons'] = CommandParser.suggestions(session_storage[user_id])

    @staticmethod
    def patient_not_selected(req, res, session_storage, user_id):
        return_text = CommandParser.return_random_from(CommandParser.error_text)

        stat = {}

        if CommandParser.find_command_in_mas(req['request']['nlu']['tokens'], 'пациент', stat):
            for entity in req['request']['nlu']['entities']:
                if entity['type'] == 'YANDEX.FIO':
                    try:
                        last_name = entity["value"]["last_name"]
                        first_name = entity["value"]["first_name"]
                        patronymic_name = entity["value"]["patronymic_name"]

                        if Hospital.change_current_patinent(first_name, last_name, patronymic_name):
                            return_text = CommandParser.return_random_from(CommandParser.answer_map['пациент']) \
                                .format(last_name.title(), first_name.title(), patronymic_name.title())

                            session_storage[user_id]['state'] = STATE.PATIENT_SELECTED
                            session_storage[user_id]['suggests'] = CommandParser.suggestion_map[STATE.PATIENT_SELECTED]
                        else:
                            return_text = 'Необходимо указать полное имя (false)'

                        break
                    except Exception as e:
                        return_text = 'Необходимо указать полное имя'

        res['response']['text'] = return_text
        res['response']['buttons'] = CommandParser.suggestions(session_storage[user_id])
        return

    @staticmethod
    def new_user(session_storage, user_id, res):
        session_storage[user_id] = {
            'state': STATE.PATIENT_NOT_SELECTED,
            'suggests': CommandParser.suggestion_map[STATE.PATIENT_NOT_SELECTED]
        }

        Hospital.change_current_doctor(user_id)

        res['response']['text'] = 'Назовите имя пациента'
        res['response']['buttons'] = CommandParser.suggestions(session_storage[user_id])
        return

    @staticmethod
    def suggestions(session):
        return [
            {'title': suggest, 'hide': True}
            for suggest in CommandParser.mix_mas(session['suggests'])
        ]

    @staticmethod
    def mix_mas(any_list):

        if any_list.__len__() < 3:
            return any_list

        print('any_list.__len__()',any_list.__len__())
        rand_int_1 = randint(0, any_list.__len__() - 1)

        rand_int_2 = randint(0, any_list.__len__() - 1)
        print(rand_int_1, rand_int_2)
        while rand_int_1 == rand_int_2:
            rand_int_2 = randint(0, any_list.__len__())

        rand_suggest = [any_list[rand_int_1], any_list[rand_int_2]]

        return rand_suggest
