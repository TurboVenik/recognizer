import urllib.request
import json
import urllib.parse

from src.apllication_server.src.enum.enums import GrammarEnum
from src.apllication_server.src.model.hospital_utils import Hospital


class StateCommandParser:
    state = GrammarEnum.STATE_1

    @staticmethod
    def parse_bytes(audio_bytes, request_params):
        # print("State before ", StateCommandParser.state)
        request_params["state"] = StateCommandParser.state.name
        recognized_command = StateCommandParser.recognize_command(audio_bytes, request_params)
        StateCommandParser.parse_command(recognized_command)
        # print("State after ", StateCommandParser.state)
        return recognized_command

    @staticmethod
    def parse_text(data):
        recognized_command = StateCommandParser.recognize_text_command(data)
        print(recognized_command)
        StateCommandParser.parse_command(recognized_command)
        return recognized_command

    @staticmethod
    def parse_command(command):
        if StateCommandParser.state == GrammarEnum.STATE_1:
            if command["type"] == "patient":
                surname_name_patronymic = command["info"].split(" ")

                if surname_name_patronymic.__len__() == 3 and \
                        Hospital.change_current_patient(surname_name_patronymic[1],
                                                        surname_name_patronymic[0],
                                                        surname_name_patronymic[2]):
                    StateCommandParser.state = GrammarEnum.STATE_2
                    return

        if StateCommandParser.state == GrammarEnum.STATE_2:
            if command["type"] == "patient":
                surname_name_patronymic = command["info"].split(" ")

                if surname_name_patronymic.__len__() == 3 and \
                        Hospital.change_current_patient(surname_name_patronymic[1],
                                                        surname_name_patronymic[0],
                                                        surname_name_patronymic[2]):
                    StateCommandParser.state = GrammarEnum.STATE_2
                    return
            if command["type"] == "start":
                if Hospital.start_visit():
                    StateCommandParser.state = GrammarEnum.STATE_3
                    return

        if StateCommandParser.state == GrammarEnum.STATE_3:
            if command["type"] == "end":
                if Hospital.end_visit():
                    StateCommandParser.state = GrammarEnum.STATE_1
                    return
            if command["type"] == "doctor":
                if Hospital.change_selected_doctor(command["info"]):
                    return
            if command["type"] == "complaint":
                if Hospital.add_complaint(command["info"]):
                    return
            if command["type"] == "dynamic":
                if Hospital.add_dynamic(command["info"]):
                    return
            if command["type"] == "destinations":
                if Hospital.add_destinations(command["info"]):
                    return
            if command["type"] == "disability":
                if Hospital.add_disability(command["info"]):
                    return

            if command["type"] == "tablet":
                if Hospital.start_tablet_edit():
                    StateCommandParser.state = GrammarEnum.STATE_4
                    return
            if command["type"] == "visit":
                if Hospital.start_watching_visit(command["info"]):
                    StateCommandParser.state = GrammarEnum.STATE_5
                    return

        if StateCommandParser.state == GrammarEnum.STATE_4:
            if command["type"] == "name":
                if Hospital.set_tablet_edit_name(command["info"]):
                    return
            if command["type"] == "dose":
                if Hospital.set_tablet_edit_dose(command["info"]):
                    return
            if command["type"] == "duration":
                if Hospital.set_tablet_edit_duration(command["info"]):
                    return
            if command["type"] == "save":
                if Hospital.save_tablet_edit():
                    StateCommandParser.state = GrammarEnum.STATE_3
                    return
            if command["type"] == "cancel":
                if Hospital.cancel_tablet_edit():
                    StateCommandParser.state = GrammarEnum.STATE_3
                    return

        if StateCommandParser.state == GrammarEnum.STATE_5:
            if command["type"] == "close":
                if Hospital.end_watching_visit():
                    StateCommandParser.state = GrammarEnum.STATE_3
                    return

    @staticmethod
    def recognize_command(audio_bytes, request_params):
        # params = "&".join([
        #     f"grammar={StateCommandParser.state.name}"
        # ])

        base_url = 'http://localhost:5001/rs/recognize?'

        params = urllib.parse.urlencode(request_params)

        url = urllib.request.Request(base_url + params, data=audio_bytes)
        url.add_header("Content-type", "application/x-www-form-urlencoded")

        response_data = urllib.request.urlopen(url).read().decode('UTF-8')
        return json.loads(response_data)

    @staticmethod
    def recognize_text_command(data):
        params = "&".join([
            f"grammar={StateCommandParser.state.name}"
        ])

        url = urllib.request.Request("http://localhost:5001/rs/text?%s" % params, data=data)
        url.add_header("Content-type", "application/x-www-form-urlencoded")

        response_data = urllib.request.urlopen(url).read().decode('UTF-8')
        return json.loads(response_data)
