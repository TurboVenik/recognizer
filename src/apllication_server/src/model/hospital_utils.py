import json
import datetime

session = [{'id': 0, 'session': '8C4DF24D117A2E2D8DD85E00AA5CAD8657FAA60F7BA2CBE4AEAD6852DBC35C4A'}]
doctor = ['Никулин Н.А.', 'Соболев Н.И.', 'Медко К.К.', 'Куликов А.М.', 'Березкин К.Е.']


class DoctorEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Doctor):
            return obj.__dict__
        if isinstance(obj, Visit):
            return obj.__dict__
        if isinstance(obj, Tablet):
            return obj.__dict__
        if isinstance(obj, Test):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)


class Hospital:
    patients = []
    current_id_patient = -1
    current_id_doctor = 0
    current_id_visit = None
    tablet_editor = None

    @staticmethod
    def start_tablet_edit():
        Hospital.tablet_editor = {
            "name": None,
            "dose": None,
            "duration": None
        }
        return True

    @staticmethod
    def save_tablet_edit():
        if Hospital.tablet_editor is not None and Hospital.tablet_editor["name"] is not None:
            name = Hospital.tablet_editor['name']
            dose = f"Дозировка {Hospital.tablet_editor['dose']}. " \
                if Hospital.tablet_editor["dose"] is not None else ""

            duration = f"Продолжительность {Hospital.tablet_editor['duration']}. " \
                if Hospital.tablet_editor['duration'] is not None else ""

            Hospital.add_tablet(f"{name}. {dose} {duration}")
            # Hospital.add_note(f"Прописано лекарство {name}. ")
            Hospital.tablet_editor = None
            return True
        return False

    @staticmethod
    def cancel_tablet_edit():
        Hospital.tablet_editor = None
        return True

    @staticmethod
    def set_tablet_edit_name(name):
        if Hospital.tablet_editor is not None:
            Hospital.tablet_editor["name"] = name
        return True

    @staticmethod
    def set_tablet_edit_dose(dose):
        if Hospital.tablet_editor is not None:
            Hospital.tablet_editor["dose"] = dose
        return True

    @staticmethod
    def set_tablet_edit_duration(duration):
        if Hospital.tablet_editor is not None:
            Hospital.tablet_editor["duration"] = duration
        return True

    @staticmethod
    def change_current_doctor(sess):
        # print('change_current_doctor', sess)
        Hospital.logged_in_doctor = session[0].get(sess)
        return True

    @staticmethod
    def add_note(story_name):
        #  print('add_note', story_name)
        if Hospital.current_id_doctor == 0:
            print(Hospital.current_id_visit)
            Hospital.patients[Hospital.current_id_patient].doctors[Hospital.current_id_doctor].visit[-1].add_story(
                story_name)
            # Hospital.current_id_visit].add_story(story_name)
        return True

    @staticmethod
    def change_selected_doctor(doctor_type):
        # print('search_doctor', doctor_name)
        flag = 0
        for t, doctor in enumerate(Hospital.patients[Hospital.current_id_patient].doctors):
            print(doctor, doctor_type)
            if doctor_type.lower() == doctor.name.lower():
                flag = 1
                Hospital.current_id_doctor = t
                # Hospital.current_id_visit = len(doctor.visit) - 1
        if flag == 0:
            return False
        return True

    @staticmethod
    def change_current_patient(second_name, first_name, patronymic_name):

        flag = 0
        for count, i in enumerate(Hospital.patients):
            if first_name.lower() == i.first_name.lower() \
                    and second_name.lower() == i.second_name.lower() \
                    and patronymic_name.lower() == i.patronymic_name.lower():
                Hospital.current_id_patient = count
                flag = 1
                A = Hospital.patients[count]
        if flag == 0:
            # A = 'Данного пациента не сучествует'
            return False  # TODO
        return True

    @staticmethod
    def end_visit():
        print('end_visit')
        Hospital.current_id_patient = -1
        Hospital.current_id_visit = None
        return True

    @staticmethod
    def start_visit():
        print('start_visit')
        A = Hospital.patients[Hospital.current_id_patient]
        first_name = A.first_name
        second_name = A.second_name
        patronymic_name = A.patronymic_name
        Hospital.change_current_patient(first_name, second_name, patronymic_name)
        Hospital.patients[Hospital.current_id_patient].doctors[0].add_visit()
        Hospital.current_id_visit = None
        return Hospital.patients[Hospital.current_id_patient]

    @staticmethod
    def collect_info_json():
        result = {'user': doctor[0],
                  'doctor_id': None,
                  'doctors': [],
                  'selected_visit': None,
                  'selected_visit_info': None,
                  'visits': [],
                  'visit_info': None,
                  'tablet_editor': None,
                  'patient_info': []
                  }

        result['tablet_editor'] = Hospital.tablet_editor
        if Hospital.current_id_patient == -1:
            return json.dumps(result, cls=DoctorEncoder, ensure_ascii=False)
        patient = Hospital.patients[Hospital.current_id_patient]
        # print(patient.analisis[0].test_info)
        heal = []
        for i, item in enumerate(patient.tablets):
            new_heal = str(i + 1) + ': '
            # print(type(item.date_of_beginning), item.date_of_beginning)

            new_heal += " {}".format(
                ', '.join(['{}'.format(item.name.capitalize())]))

            heal.append(new_heal)

        result['patient_info'] = ["Фамилия: {}".format(patient.first_name),
                           "Имя: {}".format(patient.second_name),
                           "Отчество: {}".format(patient.patronymic_name),
                           "Противопоказания: Аспириг"]
        # "Принимаемые лекарства: {}".format(', '.join(['{} c {} по {}'.format(item.name, item.date_of_beginning, item.date_of_ending) for item in patient.tablets]))]

        analises = []
        for i, item in enumerate(patient.analisis):
            # item.test_info
            text = f"{item.name}. {f'Актуальны до: ' if item.fresh_by_date!='Не проведен' else ''} {item.fresh_by_date}."
            analises.append({'id': i, 'text': text, 'state': item.status, 'date': item.date_of_reception})

        doctors = []
        for i, item in enumerate(patient.doctors):
            doctors.append({'id': i, 'name': item.name})
        result['doctors'] = doctors
        result['doctor_id'] = Hospital.current_id_doctor
        # print(Hospital.current_id_doctor

        result['selected_visit'] = Hospital.current_id_visit
        visit = patient.doctors[Hospital.current_id_doctor].visit
        visit_list = []
        for i, item in enumerate(visit):
            visit_list.append({'id': i,
                               'date': item.date,
                               'doctor': doctor[Hospital.current_id_doctor]})
        result['visits'] = visit_list
        # print(result['visits'])
        # print()

        result['visit_info'] = patient.doctors[0].visit[patient.doctors[0].visit.__len__() - 1].story

        if Hospital.current_id_visit is not None:
            result['selected_visit_info'] = visit[Hospital.current_id_visit].story
        # print(visit[Hospital.current_id_visit].story)
        # print(json.dumps(result, cls=DoctorEncoder, ensure_ascii=False))
        return json.dumps(result, cls=DoctorEncoder, ensure_ascii=False)

    @staticmethod
    def add_diagnosis(diagnosis_name):
        if Hospital.current_id_doctor == 0:
            print('add_diagnosis', diagnosis_name)
            current_patient = Hospital.patients[Hospital.current_id_patient]
            current_patient.add_diagnosis(diagnosis_name, Hospital.current_id_doctor, -1)
            # Hospital.current_id_visit)
        return True

    @staticmethod
    def add_test(name):
        Hospital.patients[Hospital.current_id_patient].add_test(name)
        Hospital.add_note(f"Назначен анализ {name}. ")
        return True

    @staticmethod
    def add_tablet(name):
        patient = Hospital.patients[Hospital.current_id_patient]
        visit = patient.doctors[0].visit
        visit[visit.__len__() - 1].add_story("tablets", name)
        return True

    @staticmethod
    def start_watching_visit(visit_id):
        patient = Hospital.patients[Hospital.current_id_patient]
        visit = patient.doctors[Hospital.current_id_doctor].visit

        if visit_id > visit.__len__():
            return False

        Hospital.current_id_visit = visit_id - 1
        return True

    @staticmethod
    def end_watching_visit():
        Hospital.current_id_visit = None
        return True

    @staticmethod
    def add_complaint(complaint):
        patient = Hospital.patients[Hospital.current_id_patient]
        visit = patient.doctors[0].visit
        visit[visit.__len__() - 1].add_story("complaints", complaint)
        return True

    @staticmethod
    def add_dynamic(dynamic):
        patient = Hospital.patients[Hospital.current_id_patient]
        visit = patient.doctors[0].visit
        visit[visit.__len__() - 1].add_story("dynamic", dynamic)
        return True

    @staticmethod
    def add_destinations(destinations):
        patient = Hospital.patients[Hospital.current_id_patient]
        visit = patient.doctors[0].visit
        visit[visit.__len__() - 1].add_story("destinations", destinations)
        return True

    @staticmethod
    def add_disability(disability):
        patient = Hospital.patients[Hospital.current_id_patient]
        visit = patient.doctors[0].visit
        visit[visit.__len__() - 1].add_story("disability", disability)
        return True

class Patient:

    def __init__(self, first_name, second_name, patronymic_name,
                 tablets=[], analisis=[], doctors=[]):
        self.first_name = first_name
        self.second_name = second_name
        self.patronymic_name = patronymic_name
        self.diagnose = []
        self.tablets = tablets
        self.analisis = analisis
        self.doctors = doctors

    def add_tablet(self, name):
        new_tablet = Tablet(name)
        self.tablets.append(new_tablet)
        return True

    def add_test(self, name):
        new_test = Test(name)
        self.analisis.append(new_test)
        return True

    def add_doctor(self, name):
        self.doctors.append(Doctor(name))
        # Hospital.current_id_doctor = len(self.doctors)
        return True

    def add_diagnosis(self, diagnose, doctor, visit):
        self.diagnose.append(diagnose)

        old_report = self.doctors[doctor].visit[visit].medical_report
        if old_report:
            new_report = "{}, {}".format(old_report, diagnose)
            self.doctors[doctor].visit[visit].medical_report = new_report
        else:
            self.doctors[doctor].visit[visit].medical_report = diagnose
        return True


class Doctor:

    def __init__(self, name, visit=[]):
        self.name = name
        self.visit = visit

    def add_visit(self):
        story = {
            "date": datetime.datetime.today().strftime("%d/%m/%Y"),
            "complaints": [],
            "dynamic": [],
            "destinations": [],
            "tablets": [],
            "disability": [],
            "privileges": [],
            "user": "Никулин Н.А.",
        }
        self.visit.append(Visit(date=datetime.datetime.today().strftime("%d/%m/%Y"), story=story))
        return True


class Visit:
    def __init__(self, story,
                 date=datetime.datetime.today().strftime("%d/%m/%Y %H.%M.%S"),
                 short_info='', medical_report=''):
        self.date = date
        self.story = story
        self.short_info = short_info
        self.medical_report = medical_report

    def add_story(self, key, value):
        self.story[key].append(value)
        return True


class Tablet:
    def __init__(self, name):
        self.name = name


class Test:
    def __init__(self, name='',
                 date_of_reception=datetime.datetime.today().strftime("%d/%m/%Y"),
                 fresh_by_date='Не проведен', test_info='', status=0):
        self.name = name
        self.date_of_reception = datetime.datetime.today().strftime("%d/%m/%Y")
        self.fresh_by_date = fresh_by_date
        self.test_info = test_info
        self.status = status
