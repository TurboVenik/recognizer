import json


class Doctors:
    Doctors = {}
    Patients = {}

    def add_patient(self, name, current_diagnose, doctors, doctor_name):
        self.Patients.update({name: {'current_diagnose': current_diagnose, 'doctors': ['Kirill']}})
        return json.dumps({name: {'current_diagnose': current_diagnose, 'doctors': ['Kirill']}})

    def add_doctor(self, name, visit):
        self.Doctors.update({name: {}})
        return json.dumps({name: {}})

    def add_visit(self, tablets, tests, date, short_info, medical_report, doctor_name, patient):
        self.Doctors[doctor_name].update(
            {patient: {'tablets': [], 'tests': [], 'date': date, 'short_info': '', 'medical_report': ''}})
        if medical_report != '':
            self.Patients[patient]['current_diagnose'] = medical_report
        if tablets != '':
            self.add_tablet(tablets, date, int(date) + 10, doctor_name, patient)
        if tests != '':
            self.add_test(tests, date, int(date) + 10, '', doctor_name, patient)
        # pass
        return json.dumps({patient: {'tablets': [], 'tests': [], 'date': date, 'short_info': '', 'medical_report': ''}})

    def add_short_info(self, doctor_name, patient, text):
        self.Doctors[doctor_name][patient]['short_info'] = text
        return json.dumps(text)

    def add_medical_report(self, doctor_name, patient, text):
        self.Doctors[doctor_name][patient]['medical_report'] = text
        # print(text)
        # print(json.dumps(text))
        return json.dumps(text)

    def add_tablet(self, name, date_of_beginning, date_of_end, doctor_name, patient):
        self.Doctors[doctor_name][patient]['tablets'].append(
            {'name_of_the_tablet': name, 'date_of_beginning': date_of_beginning, 'date_of_end': date_of_end})
        return json.dumps(
            {'name_of_the_tablet': name, 'date_of_beginning': date_of_beginning, 'date_of_end': date_of_end})

    def add_test(self, name, date_of_reception, fresh_by_date, discription, doctor_name, patient):
        self.Doctors[doctor_name][patient]['tests'].append(
            {'name_of_the_procedure': name, 'date_of_reception': date_of_reception, 'fresh_by_date': fresh_by_date,
             'discription': discription})
        return json.dumps(
            {'name_of_the_procedure': name, 'date_of_reception': date_of_reception, 'fresh_by_date': fresh_by_date,
             'discription': discription})


A = Doctors()

A.add_doctor('Kirill', '')
A.add_patient('Ivanov Ivan Ivanovich', 'volchanka', '', 'Kirill')
A.add_visit('paracetamol', 'prostate exam', '1945', '', 'DCP', 'Kirill', 'Ivanov Ivan Ivanovich')
A.add_medical_report('Kirill', 'Ivanov Ivan Ivanovich', 'Zdorov!')
# print(A.Doctors)
