from flask import Blueprint, render_template

hello_api = Blueprint('hello_api', __name__)


@hello_api.route("/")
def root():
    return render_template('index.html')

@hello_api.route("/doclk.html")
def doclk():
    return render_template('doclk.html')

@hello_api.route("/main.html")
def mainpage():
    return render_template('main.html')

