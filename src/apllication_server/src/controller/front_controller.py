from flask import Blueprint
from src.apllication_server.src.model.hospital_utils import Hospital, DoctorEncoder
import datetime, json

current_data = datetime.datetime.today().strftime("%d/%m/%Y")

card_api = Blueprint('card_api', __name__)


@card_api.route("/top/card")
def account_list():
    return Hospital.collect_info_json()


@card_api.route("/top/patients")
def patient_list():
    result = []
    for p in Hospital.patients:
        result.append("{} {} {}".format(p.first_name, p.second_name, p.patronymic_name))
    return json.dumps(result, cls=DoctorEncoder, ensure_ascii=False)
