from flask import Blueprint, request
import json
import logging

from src.apllication_server.src.enum.enums import STATE
from src.apllication_server.src.service.parse_command_service import CommandParser

alisa_api = Blueprint('alisa_api', __name__)

logging.basicConfig(level=logging.DEBUG)

# Хранилище данных о сессиях.
sessionStorage = {}


@alisa_api.route("/top/alisa", methods=["POST"])
def main():
    response = {
        "version": request.json['version'],
        "session": request.json['session'],
        "response": {
            "end_session": False
        }
    }

    handle_dialog(request.json, response)

    # logging.info('Response: %r', response)

    return json.dumps(
        response,
        ensure_ascii=False,
        indent=2
    )

# Функция для непосредственной обработки диалога.
def handle_dialog(req, res):
    user_id = req['session']['user_id']

    # try:
    if req['session']['new']:
        CommandParser.new_user(sessionStorage, user_id, res)
        return
    #(sessionStorage[user_id]['state'])
    if sessionStorage[user_id]['state'] == STATE.PATIENT_NOT_SELECTED:
        CommandParser.patient_not_selected(req, res, sessionStorage, user_id)
        return

    if sessionStorage[user_id]['state'] == STATE.PATIENT_SELECTED:
        CommandParser.patient_selected(req, res, sessionStorage, user_id)
        return

    if sessionStorage[user_id]['state'] == STATE.VISIT_STARTED:
        CommandParser.visit_started(req, res, sessionStorage, user_id)
        return
    # except Exception as e:
    #     #(str(e))
    #     res['response']['text'] = 'Давай, придумай что-нибудь получше'
    #     res['response']['buttons'] = CommandParser.suggestions(sessionStorage[user_id])
    #     return
