import json

from flask import Blueprint, request

from src.apllication_server.src.service.sphinx_command_parser import StateCommandParser

sphinx_api = Blueprint('sphinx_api', __name__)


@sphinx_api.route('/as/recognize', methods=['POST'])
def recognize():
    audio_bytes = request.get_data()

    params = {
        "stop_phrase": request.values["stop_phrase"]
    }

    print("/as/recognize")
    print("\tparams:", params)
    print("\taudio audio_bytes[10]:", audio_bytes[0: 10])
    answer = StateCommandParser.parse_bytes(audio_bytes, params)
    response = json.dumps(answer, ensure_ascii=False)
    print("\tresponse", response)
    return response


@sphinx_api.route('/as/text', methods=['POST'])
def text_command():
    answer = StateCommandParser.parse_text(request.get_data())
    return json.dumps(answer, ensure_ascii=False)
