from enum import Enum


class STATE(Enum):
    INIT = 0
    PATIENT_NOT_SELECTED = 1
    PATIENT_SELECTED = 2
    VISIT_STARTED = 3


class GrammarEnum(Enum):
    STATE_1 = 0
    STATE_2 = 1
    STATE_3 = 2
    STATE_4 = 3
    STATE_5 = 4
