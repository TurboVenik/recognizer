import argparse
import urllib.request
import json


def init_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-tc", "--text_command",
                        help="text command which will be send (default пациент деревянко артём алексеевич)")
    parser.add_argument("-g", "--grammar", help="grammar which will be send to server (default STATE_1)")
    parser.add_argument("-p", "--port", help="port of recognition server (default 5001)")
    return parser


def parse_args(args, edit_settings):

    if args.text_command:
        edit_settings["text_command"] = args.text_command
    if args.grammar:
        edit_settings["grammar"] = args.grammar
    if args.port:
        edit_settings["port"] = args.port


settings = {
    "text_command": "пациент деревянко артём алексеевич",
    "grammar": "STATE_1",
    "port": 5001
}

parser = init_parser()
args = parser.parse_args()
parse_args(args, settings)

params = "&".join([
    f"grammar={settings['grammar']}"
])

url = urllib.request.Request(f"http://localhost:{settings['port']}/rs/text?%s" % params,
                             data=bytearray(settings["text_command"], "UTF-8"))
url.add_header("Content-type", "application/x-www-form-urlencoded")

responseData = urllib.request.urlopen(url).read().decode('UTF-8')
decodedData = json.loads(responseData)

print(decodedData)
