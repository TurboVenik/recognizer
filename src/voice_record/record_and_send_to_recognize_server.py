import argparse

import pyaudio
import urllib.request
import json


def init_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-g", "--grammar", help="grammar which will be send to server (default STATE_1)")
    parser.add_argument("-p", "--port", help="port of recognition server (default 5001)")
    parser.add_argument("-rs", "--record_second", help="time of voice record in seconds (default 3)")
    return parser


def parse_args(args, edit_settings):
    if args.grammar:
        edit_settings["grammar"] = args.grammar
    if args.port:
        edit_settings["port"] = args.port
    if args.record_second:
        edit_settings["record_second"] = int(args.record_second)


settings = {
    "grammar": "STATE_1",
    "port": 5001,
    "record_second": 3
}

parser = init_parser()
args = parser.parse_args()
parse_args(args, settings)

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
RECORD_SECONDS = settings["record_second"]
WAVE_OUTPUT_FILENAME = "output.wav"

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

print("* recording")

frames = []

for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    data = stream.read(CHUNK)
    frames.append(data)

print("* done recording")

stream.stop_stream()
stream.close()
p.terminate()

params = "&".join([
    f"grammar={settings['grammar']}"
])

url = urllib.request.Request(f"http://localhost:{settings['port']}/rs/recognize?%s" % params, data=b''.join(frames))
url.add_header("Content-type", "application/x-www-form-urlencoded")

responseData = urllib.request.urlopen(url).read().decode('UTF-8')
decodedData = json.loads(responseData)

print(decodedData)
