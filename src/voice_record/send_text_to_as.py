import argparse
import urllib.request
import json


SUPER_COMANDA_VVEDI_SUDA_I_BYDET_HOROWO = "пациент иванов михаил николаевич"

def init_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-tc", "--text_command",
                        help="text command which will be send (default пациент деревянко артём алексеевич)")
    parser.add_argument("-p", "--port", help="port of application server (default 5002)")
    return parser


def parse_args(args, edit_settings):
    if args.text_command:
        edit_settings["text_command"] = args.text_command
    if args.port:
        edit_settings["port"] = args.port


settings = {
    "text_command": "пациент иванов михаил николаевич",
    "port": 5002
}


parser = init_parser()
args = parser.parse_args()
parse_args(args, settings)

url = urllib.request.Request(f"http://localhost:{settings['port']}/as/text",
                             data=bytearray(settings["text_command"], "UTF-8"))
url.add_header("Content-type", "application/x-www-form-urlencoded")

responseData = urllib.request.urlopen(url).read().decode('UTF-8')
decodedData = json.loads(responseData)

print(decodedData)
