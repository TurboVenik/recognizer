Запуск 

    record_and_send_to_application_server.py 
        - запись звука и его отправка на обработку на application server (обычный режим работы, меняется front)
        
    record_and_send_to_recognize_server.py
        - запись звука и его отправка на обработку на recognition server (затестировать команду, не меняется front)    
        
    send_text_to_as.py.py
        - отправка текста на обработку на application server (обычный режим работы, меняется front)
        
    send_text_to_rs.py
        - отправка текста на обработку на recognition server (затестировать команду, не меняется front)
        
Параметры
    
    Чтобы не менять код скриптов, можно пользоваться параметрами командной строки (посмотреть их список -h)
    (пример) python send_text_to_rs.py -h
    
    send_text_to_as.py.py и send_text_to_rs.py (-tc - текст команды)
    
    record_and_send_to_recognize_server.py и send_text_to_rs.py (-g - грамматика, которая будет использована) 
    
    record_and_send_to_application_server.py и record_and_send_to_recognize_server.py (-rs 3 время записи голоса в секундах)
    
    у всех -p порт, куда посылать сообщение
    
Примеры
    
    python send_text_to_rs.py -tc "пациент иванов михаил никлаевич"    
    python send_text_to_rs.py -tc "продолжительность две недели" -g STATE_4 -p 5001
                 
    python send_text_to_as.py -tc "пациент иванов михаил николаевич" -p 5002 
    python send_text_to_as.py -tc "начать"            
    
    python record_and_send_to_recognize_server.py -g STATE_4 -rs 2 -p 5001 (название алаьгин)
    
    python record_and_send_to_application_server.py -p 5002 -rs 3 (пациент иванов михаил николаевич)
