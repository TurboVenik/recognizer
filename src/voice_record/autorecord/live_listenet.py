import sys, os

import pyaudio
from pocketsphinx import *
from sphinxbase import *
# pocketsphinx_continuous -infile test.wav  -hmm zero_ru.cd_cont_4000 -dict ru.dic -lm ru.lm -remove_noise no


modeldir = "../resources/zero_ru_cont_8k_v3"

# Create a decoder with certain model
# config = Decoder.default_config()
# config.set_string('-hmm', os.path.join(modeldir, 'zero_ru.cd_cont_4000'))
# config.set_string('-dict', os.path.join(modeldir, 'ru.dic'))
# # config.set_string('-lm', os.path.join(modeldir, 'ru.lm'))
# config.set_string('-keyphrase', 'начать')
# config.set_float('-kws_threshold', 1e-10)

config = Decoder.default_config()
config.set_string('-hmm', os.path.join(modeldir, 'zero_ru.cd_cont_4000'))
config.set_string('-dict', os.path.join(modeldir, 'ru.dic'))
config.set_string('-keyphrase', 'запись')
config.set_float('-kws_threshold', 1e-10)
# config.set_string('-logfn', '/dev/null')


# Open file to read the data
stream = open(os.path.join("", "output2.ogg"), "rb")

# Alternatively you can read from microphone
# import pyaudio
# 
# p = pyaudio.PyAudio()
# stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=1024)
# stream.start_stream()

# Process audio chunk by chunk. On keyphrase detected perform action and restart search
decoder = Decoder(config)
decoder.start_utt()
print("...:::Started:::...")
while True:
    buf = stream.read(1024)
    if buf:
         answer = decoder.process_raw(buf, False, False)
    else:
         break
    if decoder.hyp() != None:
        print ([(seg.word, seg.prob, seg.start_frame, seg.end_frame) for seg in decoder.seg()])
        print ("Detected keyphrase, restarting search")
        decoder.end_utt()
