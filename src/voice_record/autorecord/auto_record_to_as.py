import json
import time
import urllib.request
import urllib.parse
from queue import Queue
from threading import Thread

from pocketsphinx import Decoder

import os

import argparse

import pyaudio
from pydub import AudioSegment


class SpeechToTextThread(Thread):

    def __init__(self, settings):
        Thread.__init__(self)
        self.command_queue = Queue()

        self.CHANNELS = settings["CHANNELS"]
        self.RATE = settings["RATE"]

        self.index = 0
        self.sleep_time = 0.02
        self.stop_phrase = settings["stop_phrase"]

    def run(self):
        while True:
            try:
                self.process_command(self.get_new_command())
            except Exception as e:
                print(e)
            time.sleep(self.sleep_time)

    def process_command(self, command):
        if command is None:
            return

        decoded_data = self.to_text(command)
        name = f"_{self.index}_{decoded_data['type']}.wav"
        self.save_wav(command, name)
        print(f"\tdecoded command {decoded_data}; file {name} ")

    def save_wav(self, command, name):
        self.index += 1
        sound = AudioSegment(b''.join(command), sample_width=2, frame_rate=self.RATE,
                             channels=self.CHANNELS)
        sound.export(f"audio/{name}", format="wav")

    def to_text(self, command):

        base_url = 'http://localhost:5002/as/recognize?'
        params = urllib.parse.urlencode({'stop_phrase': self.stop_phrase})

        data = b''.join(command)
        print(data[0:10])
        url = urllib.request.Request(base_url + params, data=data)
        url.add_header("Content-type", "application/x-www-form-urlencoded")

        response_data = urllib.request.urlopen(url).read().decode('UTF-8')
        return json.loads(response_data)

        # try:
        #     params = "&".join([
        #         f"grammar=STATE_3"
        #     ])
        #
        #     url = urllib.request.Request(f"http://localhost:5001/rs/recognize?%s" % params,
        #                                  data=b''.join(command))
        #     url.add_header("Content-type", "application/x-www-form-urlencoded")
        #
        #     response_data = urllib.request.urlopen(url).read().decode('UTF-8')
        #     return json.loads(response_data)
        # except Exception as e:
        #     # print(e)
        #     pass
        # return {"type": "Error"}

    def get_new_command(self):

        if not self.command_queue.empty():
            return self.command_queue.get()
        return None

    def put_new_command(self, command):
        self.command_queue.put(command)


class AudioReaderThread(Thread):

    def __init__(self, settings):
        Thread.__init__(self)
        self.name = "audioReader"
        self.frames = Queue()

        self.CHUNK = settings["CHUNK"]
        self.FORMAT = settings["FORMAT"]
        self.CHANNELS = settings["CHANNELS"]
        self.RATE = settings["RATE"]
        self.RECORD_SECONDS = settings["record_second"]

        self.p = pyaudio.PyAudio()

        self.stream = self.p.open(format=self.FORMAT,
                                  channels=self.CHANNELS,
                                  rate=self.RATE,
                                  input=True,
                                  frames_per_buffer=self.CHUNK)

    def run(self):
        while True:
            fr = self.stream.read(self.CHUNK)
            self.frames.put(fr)

    def get_new_frames(self, max_frames_count):
        answer = []
        for i in range(max_frames_count):
            if not self.frames.empty():
                answer.append(self.frames.get())
            else:
                break
        return answer


class AudioParser(Thread):

    def __init__(self, settings, decoder):
        Thread.__init__(self)
        self.decoder = decoder
        self.reader = AudioReaderThread(settings)

        self.speech_to_text = SpeechToTextThread(settings)

        self.CHUNK = settings["CHUNK"]
        self.FORMAT = settings["FORMAT"]
        self.CHANNELS = settings["CHANNELS"]
        self.RATE = settings["RATE"]
        self.RECORD_SECONDS = settings["record_second"]

        self.sleep_time = 0.03

    def process_command(self, command):
        self.speech_to_text.put_new_command(command)

    def run(self):

        key_word_found = False
        audio_buffer = []
        self.reader.start()
        self.speech_to_text.start()
        print("...::: WAIT FOR KEY :::...")
        while True:
            if not key_word_found:
                new_frames = self.reader.get_new_frames(1000)
                audio_buffer = audio_buffer + new_frames

                decoded_data = self.decoder.recognize(b''.join(audio_buffer))

                if len(decoded_data) != 0 and decoded_data[0]["word"] == "начать запись":
                    print(decoded_data)
                    # key_phrase_end = int(self.RATE / self.CHUNK * decoded_data[0]["end_frame"] / 100)
                    print(len(audio_buffer))
                    # audio_buffer = audio_buffer[key_phrase_end:]
                    audio_buffer = []
                    print(len(audio_buffer))
                    key_word_found = True
                    print("...::: COMMAND :::...")
                else:
                    if len(audio_buffer) > self.RATE / self.CHUNK * 4:
                        audio_buffer = audio_buffer[len(audio_buffer) // 2:  len(audio_buffer)]
                    time.sleep(self.sleep_time)
            else:
                new_frames = self.reader.get_new_frames(1000)

                audio_buffer = audio_buffer + new_frames

                decoded_data = self.decoder.recognize(b''.join(audio_buffer))

                if len(decoded_data) != 0 and decoded_data[0]["word"] == "закончить запись":
                    print(decoded_data)
                    # key_phrase_start = math.ceil(self.RATE * decoded_data[0]["start_frame"] / self.CHUNK / 100)

                    # command = audio_buffer[:key_phrase_start]
                    command = audio_buffer
                    audio_buffer = []
                    self.process_command(command)
                    key_word_found = False
                    print("...::: COMMAND :::...")

                if len(audio_buffer) > self.RATE / self.CHUNK * 8:
                    command = audio_buffer
                    self.process_command(command)
                    audio_buffer = []
                    key_word_found = False
                    print("...::: WAIT FOR KEY :::...")
                time.sleep(self.sleep_time)


class KeyPhraseDecoder:

    def __init__(self, model_path):
        self.model_path = model_path

        self.config = Decoder.default_config()
        self.config.set_string('-hmm', os.path.join(self.model_path, 'zero_ru.cd_cont_4000'))
        self.config.set_string('-dict', os.path.join(self.model_path, 'ru.dic'))
        # self.config.set_string('-keyphrase', 'запись')
        # self.config.set_float('-kws_threshold', 1e-20)
        self.config.set_string('-kws',  os.path.join(self.model_path, 'keywords.key'))
        self.config.set_string('-logfn', '/dev/null')

        self.decoder = Decoder(self.config)

    def recognize(self, buf):
        self.decoder.start_utt()
        self.decoder.process_raw(buf, False, True)
        self.decoder.end_utt()

        # print(self.decoder.hyp())

        # for item in self.decoder.nbest():
        #     print(f"{item.hypstr}  {item.score}")

        answer = []
        for seg in self.decoder.seg():
            answer_item = {
                "word": seg.word,
                "start_frame": seg.start_frame,
                "end_frame": seg.end_frame
            }
            answer.append(answer_item)

        answer = sorted(answer, key=lambda k: k['start_frame'])
        return answer


def init_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", help="port of application server (default 5002)")
    parser.add_argument("-rs", "--record_second", help="time of voice record in seconds (default 3)")
    return parser


def parse_args(args, edit_settings):
    if args.port:
        edit_settings["port"] = args.port
    if args.record_second:
        edit_settings["record_second"] = int(args.record_second)


settings = {
    "port": 5002,
    "record_second": 1,
    "CHUNK": 8,
    "FORMAT": pyaudio.paInt16,
    "CHANNELS": 1,
    "RATE": 16000,
    "stop_phrase": "закончить запись"
}

parser = init_parser()
parse_args(parser.parse_args(), settings)

print("...::: INIT DECODER START:::...")
audio_parser = AudioParser(settings, KeyPhraseDecoder("../resources/zero_ru_cont_8k_v3"))
print("...::: INIT DECODER END:::...")

audio_parser.start()

# CHUNK = settings["CHUNK"]
# FORMAT = settings["FORMAT"]
# CHANNELS = settings["CHANNELS"]
# RATE = settings["RATE"]
# RECORD_SECONDS = settings["record_second"]
#
# p = pyaudio.PyAudio()
#
# stream = p.open(format=FORMAT,
#                 channels=CHANNELS,
#                 rate=RATE,
#                 input=True,
#                 frames_per_buffer=CHUNK)
# frames1 = Queue()
# for i in range(0, int(RATE / CHUNK * 3)):
#     data = stream.read(CHUNK)
#     frames1.put(data)
#
# frames = []
# while not frames1.empty():
#     frames.append(frames1.get())
#
# sound = AudioSegment(b''.join(frames), sample_width=2, frame_rate=RATE, channels=CHANNELS)
#
#
# sound.export(f"part{12}.wav", format="wav")
