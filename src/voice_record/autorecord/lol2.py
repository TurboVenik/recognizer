from pocketsphinx import Decoder

import os
from pydub import AudioSegment
import argparse

import pyaudio
import math


class MyKeyphraseDecoder:

    def __init__(self, model_path):
        self.model_path = model_path

        self.config = Decoder.default_config()
        self.config.set_string('-hmm', os.path.join(self.model_path, 'zero_ru.cd_cont_4000'))
        self.config.set_string('-dict', os.path.join(self.model_path, 'ru.dic'))
        self.config.set_string('-keyphrase', 'начать')
        self.config.set_float('-kws_threshold', 1e-20)

        self.decoder = Decoder(self.config)

    def recognize(self, buf):

        self.decoder.start_utt()
        self.decoder.process_raw(buf, False, True)
        self.decoder.end_utt()

        print(self.decoder.hyp())

        for item in self.decoder.nbest():
            print(f"{item.hypstr}  {item.score}")

        answer = []
        for seg in self.decoder.seg():
            answer_item = {
                "word": seg.word,
                "start_frame": seg.start_frame,
                "end_frame": seg.end_frame
            }
            answer.append(answer_item)

        answer = sorted(answer, key=lambda k: k['start_frame'])
        return answer


def init_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", help="port of application server (default 5002)")
    parser.add_argument("-rs", "--record_second", help="time of voice record in seconds (default 3)")
    return parser


def parse_args(args, edit_settings):
    if args.port:
        edit_settings["port"] = args.port
    if args.record_second:
        edit_settings["record_second"] = int(args.record_second)


print("...::: INIT DECODER START:::...")
decoder = MyKeyphraseDecoder("../resources/zero_ru_cont_8k_v3")
print("...::: INIT DECODER END:::...")

settings = {
    "port": 5002,
    "record_second": 5
}

parser = init_parser()
args = parser.parse_args()
parse_args(args, settings)

CHUNK = 8
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
RECORD_SECONDS = settings["record_second"]

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

print("* recording")

frames = []

for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    data = stream.read(CHUNK)
    frames.append(data)

print("* done recording")
print(frames.__len__())
print(frames)
stream.stop_stream()
stream.close()
p.terminate()

# for i in range(2):
decoded_data = decoder.recognize(b''.join(frames))
print(decoded_data)

sound = AudioSegment(b''.join(frames), sample_width=2, frame_rate=RATE, channels=CHANNELS)


for i in range(len(decoded_data)):
    start = RATE / CHUNK * decoded_data[i]["end_frame"] / 100

    if i == len(decoded_data) - 1:
        end = len(frames)
    else:
        end = RATE / CHUNK * decoded_data[i + 1]["start_frame"] / 100

    # first_half = frames[math.trunc(start):math.ceil(end)]
    # first_half = frames[math.ceil(start):math.trunc(end)]
    first_half = frames[int(start):int(end)]

    sound = AudioSegment(b''.join(first_half), sample_width=2, frame_rate=RATE, channels=CHANNELS)

    # create a new file "first_half.mp3":
    sound.export(f"part{i}.wav", format="wav")
