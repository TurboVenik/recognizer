from io import BytesIO

import pyaudio
import wave
import soundfile as sf
# from kaitaistruct import  KaitaiStream, BytesIO
from kaitaistruct import *


# FORMAT = pyaudio.paInt8
from src.voice_record.autorecord.ogg import Ogg

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
CHUNK = 1024
RECORD_SECONDS = 3
WAVE_OUTPUT_FILENAME = "speech.wav"

audio = pyaudio.PyAudio()

# start Recording
stream = audio.open(format=FORMAT, channels=CHANNELS,
                    rate=RATE, input=True,
                    frames_per_buffer=CHUNK)
print("recording...")
frames = []

for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    data = stream.read(CHUNK)
    frames.append(data)
print("finished recording")

# stop Recording
stream.stop_stream()
stream.close()
audio.terminate()

waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
waveFile.setnchannels(CHANNELS)
waveFile.setsampwidth(audio.get_sample_size(FORMAT))
waveFile.setframerate(RATE)
waveFile.writeframes(b''.join(frames))
waveFile.close()

# generate some audio data
# data = np.random.randn(1000)
# create an audio "file"
# file = BytesIO()
# write the audio data to the file
# sf.write(b''.join(frames), file, 16000, format='ogg')
# # get the file content
# content = bytes(file.getbuffer())

# data = Ogg.from_file("path/to/local/file.['ogg', 'ogv', 'oga', 'spx', 'ogx']")

# raw = b''.join(frames)
# raw = b'\x00'
#
# BytesIO(raw)
# KaitaiStream(BytesIO(raw))
# newdata = Ogg(KaitaiStream(BytesIO(raw)))
# print(b''.join(newdata))
# data, samplerate = sf.read('file.wav')
# sf.write('new_file.ogg', data, samplerate)
# print(5)