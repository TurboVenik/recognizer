from pocketsphinx import LiveSpeech

import os
from pocketsphinx import LiveSpeech

model_path = "resources/zero_ru_cont_8k_v3"

# speech = LiveSpeech(
#     verbose=False,
#     sampling_rate=16000,
#     buffer_size=2048,
#     no_search=False,
#     full_utt=False,
#     hmm=os.path.join(model_path, 'en-us'),
#     lm=os.path.join(model_path, 'en-us.lm.bin'),
#     dic=os.path.join(model_path, 'cmudict-en-us.dict')
# )

speech = LiveSpeech(
    verbose=False,
    sampling_rate=16000,
    buffer_size=2048,
    no_search=False,
    full_utt=False,
    hmm=os.path.join(model_path, 'zero_ru.cd_cont_4000'),
    dic=os.path.join(model_path, 'ru.dic'),
    lm=False,
    kws_threshold=1e-10,
    keyphrase='начать'
)

# for phrase in speech:
#     print(phrase)
#
# speech = LiveSpeech(lm=False, keyphrase='forward', kws_threshold=1e-20)

print("Start")
for phrase in speech:
    print(phrase.segments(detailed=True))