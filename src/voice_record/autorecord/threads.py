# -*- coding: utf-8 -*-
import random
import time
from queue import LifoQueue
from threading import Thread


class MyThread(Thread):
    """
    A threading example
    """

    def __init__(self, name):
        """Инициализация потока"""
        Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        amount = random.randint(3, 15)
        time.sleep(amount)
        msg = "%s is running" % self.name
        print(msg)


def create_threads():
    """
    Создаем группу потоков
    """
    for i in range(5):
        name = "Thread #%s" % (i + 1)
        my_thread = MyThread(name)
        my_thread.start()


class ReadThread(Thread):

    def __init__(self, myQueue):
        Thread.__init__(self)
        self.mylifo = myQueue

    def run(self):
        while True:
            if not self.mylifo.empty():
                print(self.mylifo.get())


if __name__ == "__main__":
    lifo = LifoQueue()
    reader = ReadThread(lifo)
    reader.start()
    i = 0

    while True:
        lifo.put(i)
        i += 1
        time.sleep(1)

