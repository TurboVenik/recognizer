import unittest

from src.recognition_server.app.recognizer.parse_utils.parse_text_numbers_to_numbers import ParseTextNumbersToNumbers


class TestParseTextNumbersToNumbers(unittest.TestCase):
    parser = ParseTextNumbersToNumbers()

    def test_word_to_num(self):

        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('сто семь тысяч двести семьдесят пять'), 107275)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num(
            'пять миллиардов двести двадцать две тысячи двести двадцать три точка девять семь семь'),
            5000222223.977)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('семь'), 7)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('двести семь тысяч двести семьдесят пять'),
                         207275)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('двести тысяч двести семьдесят пять'), 200275)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('шесть тысяч двести семьдесят девять'), 6279)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('шесть тысяч двести пять'), 6205)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('два миллиона шесть тысяч двести четыре'), 2006204)
        self.assertEqual(
            ParseTextNumbersToNumbers.word_to_num('два миллиона шесть тысяч двести семьдесят восемь'), 2006278)

        # Simple tests with one word:
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('ноль'), 0)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('одно'), 1)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('двух'), 2)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('трёх'), 3)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('четыре'), 4)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('пятью'), 5)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('шесть'), 6)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('семь'), 7)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('восьми'), 8)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('девять'), 9)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('десять'), 10)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('одиннадцати'), 11)

        # Tests with key word "точка":
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('сорок два точка семь'), 42.7)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('шестнадцать точка девять'), 16.9)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('миллион точка два семь шесть пять три два два '),
                         1000000.2765322)
        self.assertEqual(ParseTextNumbersToNumbers.word_to_num('семьдесят четыре точка два четыре'), 74.24)
