import unittest

from src.recognition_server.app.recognizer.parse_utils.lex_yacc.lex_parser import lexer


class TestLexTokensParse(unittest.TestCase):

    def test_parse_tokens(self):
        commands = [
            {
                "examples": ["закончить запись", "закончи запись", "закончи записи"],
                "tokens": []
            },
            {
                "examples": ["пациент", "пациенты"],
                "tokens": ["PATIENT"]
            },
            {
                "examples": ["начать", "начать приём", "начать приёмы"],
                "tokens": ["START"]
            },
            {
                "examples": ["специалист", "специалисты", "специалистов"],
                "tokens": ["DOCTOR"]
            },
            {
                "examples": ["посещение", "посещения", "посещений"],
                "tokens": ["VISIT"]
            },
            {
                "examples": ["лекарство", "лекарства", "лекарств"],
                "tokens": ["TABLET"]
            },
            {
                "examples": ["завершить прием", "заверши прием", "закончи приемы"],
                "tokens": ["END"]
            },
            {
                "examples": ["жалобы", "жалоба"],
                "tokens": ["COMPLAINT"]
            },
            {
                "examples": ["наблюдение", "наблюдения"],
                "tokens": ["DYNAMIC"]
            },
            {
                "examples": ["назначение", "назначения"],
                "tokens": ["DESTINATIONS"]
            },
            {
                "examples": ["справка", "справки"],
                "tokens": ["DISABILITY"]
            },
            {
                "examples": ["название", "названия"],
                "tokens": ["NAME"]
            },
            {
                "examples": ["дозировка", "дозировки"],
                "tokens": ["DOSE"]
            },
            {
                "examples": ["продолжительность", "продолжительности"],
                "tokens": ["DURATION"]
            },
            {
                "examples": ["закрыть", "закрыто"],
                "tokens": ["CLOSE"]
            },
            {
                "examples": ["сохранить изменения", "сохрани изменения"],
                "tokens": ["SAVE"]
            },
            {
                "examples": ["отменить изменения", "отмени изменения"],
                "tokens": ["CANCEL"]
            },
            {
                "examples": ["слово", "фраза", "анальгин", "боль"],
                "tokens": ["WORD"]
            }
        ]

        for command in commands:
            for example in command["examples"]:
                lexer.input(example)
                tokens = []

                tok = lexer.token()
                while tok:
                    tokens.append(tok)
                    tok = lexer.token()
                self.assertEqual( len(command["tokens"]), len(tokens))

                for i in range(len(tokens)):
                    self.assertEqual(command["tokens"][i], tokens[i].type)


if __name__ == '__main__':
    unittest.main()
