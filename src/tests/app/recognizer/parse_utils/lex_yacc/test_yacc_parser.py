import unittest

from src.recognition_server.app.recognizer.parse_utils.lex_yacc.yacc_parser import parser


class TestLexTokensParse(unittest.TestCase):

    def test_parse_tokens(self):
        commands = [
            {
                "example": "пациент иванов иван иванович закончить запись",
                "answer":
                    {
                        'type': 'patient',
                        'info': 'иванов иван иванович',
                        'last': 'иванов',
                        'first': 'иван',
                        'patronymic': 'иванович'
                    }
            },
            {
                "example": "пациенты иванов михаил николаевич закончи запись",
                "answer":
                    {
                        'type': 'patient',
                        'info': 'иванов михаил николаевич',
                        'last': 'иванов',
                        'first': 'михаил',
                        'patronymic': 'николаевич'
                    }
            },
            {
                "example": "пациент иванов михаил николаевич",
                "answer":
                    {
                        'type': 'patient',
                        'info': 'иванов михаил николаевич',
                        'last': 'иванов',
                        'first': 'михаил',
                        'patronymic': 'николаевич'
                    }
            },
            {
                "example": "начать закончить запись",
                "answer":
                    {
                        'type': 'start',
                        'info': None
                    }
            },
            {
                "example": "начать прием",
                "answer":
                    {
                        'type': 'start',
                        'info': None
                    }
            },
            {
                "example": "начать приём",
                "answer":
                    {
                        'type': 'start',
                        'info': None
                    }
            },
            {
                "example": "специалист хирург",
                "answer":
                    {
                        'type': 'doctor',
                        'info': 'хирург'
                    }
            },
            {
                "example": "посещение 1",
                "answer":
                    {
                        'type': 'visit',
                        'info': '1'
                    }
            },
            {
                "example": "посещение 20",
                "answer":
                    {
                        'type': 'visit',
                        'info': '20'
                    }
            },
            {
                "example": "лекарство",
                "answer":
                    {
                        'type': 'tablet',
                        'info': None
                    }
            },
            {
                "example": "завершить прием",
                "answer":
                    {
                        'type': 'end',
                        'info': None
                    }
            },
            {
                "example": "завершить прием",
                "answer":
                    {
                        'type': 'end',
                        'info': None
                    }
            },
            {
                "example": "жалоба резкая головная боль",
                "answer":
                    {
                        'type': 'complaint',
                        'info': 'резкая головная боль'
                    }
            },
            {
                "example": "наблюдение улучшие анализа крови",
                "answer":
                    {
                        'type': 'dynamic',
                        'info': 'улучшие анализа крови'
                    }
            },
            {
                "example": "назначение консультация хирурга",
                "answer":
                    {
                        'type': 'destinations',
                        'info': 'консультация хирурга'
                    }
            },
            {
                "example": "справка нетрудоспособность 2 дня",
                "answer":
                    {
                        'type': 'disability',
                        'info': 'нетрудоспособность 2 дня'
                    }
            },
            {
                "example": "название анальгин",
                "answer":
                    {
                        'type': 'name',
                        'info': 'анальгин'
                    }
            },
            {
                "example": "дозировка 1 таблетка в день",
                "answer":
                    {
                        'type': 'dose',
                        'info': '1 таблетка в день'
                    }
            },
            {
                "example": "продолжительность 1 день",
                "answer":
                    {
                        'type': 'duration',
                        'info': '1 день'
                    }
            },
            {
                "example": "сохранить изменения",
                "answer":
                    {
                        'type': 'save',
                        'info': None
                    }
            },
            {
                "example": "отменить изменения",
                "answer":
                    {
                        'type': 'cancel',
                        'info': None
                    }
            },
            {
                "example": "закрыть",
                "answer":
                    {
                        'type': 'close',
                        'info': None
                    }
            },
            {
                "example": "жалоба закончить запись",
                "answer": None
            }
        ]

        for command in commands:
            self.assertEqual(command["answer"], parser.parse(command["example"]))


if __name__ == '__main__':
    unittest.main()
