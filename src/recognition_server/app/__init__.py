from flask import Flask


def create_app():
    app = Flask(__name__)
    app.config.from_object("src.recognition_server.config.Config")

    from src.recognition_server.app.recognizer.controllers import module

    app.register_blueprint(module)

    return app
