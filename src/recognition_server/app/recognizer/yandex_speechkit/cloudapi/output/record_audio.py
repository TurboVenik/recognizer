import argparse
from threading import Thread
from queue import Queue

import pyaudio


class AudioReaderThread(Thread):
    def __init__(self, settings):
        Thread.__init__(self)
        self.name = "audioReader"
        self.frames = Queue()

        self.CHUNK = settings["CHUNK"]
        self.FORMAT = settings["FORMAT"]
        self.CHANNELS = settings["CHANNELS"]
        self.RATE = settings["RATE"]
        self.RECORD_SECONDS = settings["record_second"]

        self.p = pyaudio.PyAudio()

        self.stream = self.p.open(format=self.FORMAT,
                                  channels=self.CHANNELS,
                                  rate=self.RATE,
                                  input=True,
                                  frames_per_buffer=self.CHUNK)

    def run(self):
        while True:
            fr = self.stream.read(self.CHUNK)
            self.frames.put(fr)

    def get_new_frames(self, max_frames_count):
        answer = []
        for i in range(max_frames_count):
            if not self.frames.empty():
                answer.append(self.frames.get())
            else:
                break
        return answer


class AudioParser(Thread):

    def __init__(self, settings, decoder):
        Thread.__init__(self)
        self.decoder = decoder
        self.reader = AudioReaderThread(settings)
        self.CHUNK = settings["CHUNK"]
        self.FORMAT = settings["FORMAT"]
        self.CHANNELS = settings["CHANNELS"]
        self.RATE = settings["RATE"]
        self.RECORD_SECONDS = settings["record_second"]

        self.sleep_time = 0.03

    def run(self):
        self.reader.start()
        while True:
            new_frames = self.reader.get_new_frames(1000)



