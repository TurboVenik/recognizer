# coding=utf8
import argparse
from threading import Thread
from queue import Queue
import grpc
import pyaudio
# from cloudapi.output.yandex.cloud.ai.stt.v2 import stt_service_pb2
# from cloudapi.output.yandex.cloud.ai.stt.v2 import stt_service_pb2_grpc
from src.recognition_server.app.recognizer.yandex_speechkit.cloudapi.output.yandex.cloud.ai.stt.v2 import stt_service_pb2
from src.recognition_server.app.recognizer.yandex_speechkit.cloudapi.output.yandex.cloud.ai.stt.v2 import stt_service_pb2_grpc
CHUNK_SIZE = 16000


def gen(folder_id, audio_file_name, reader, settings):
    # Задать настройки распознавания.
    specification = stt_service_pb2.RecognitionSpec(
        language_code='ru-RU',
        profanity_filter=True,
        model='general',
        partial_results=True,
        audio_encoding='LINEAR16_PCM',
        sample_rate_hertz=settings["RATE"]
    )
    streaming_config = stt_service_pb2.RecognitionConfig(specification=specification, folder_id=folder_id)

    # Отправить сообщение с настройками распознавания.
    yield stt_service_pb2.StreamingRecognitionRequest(config=streaming_config)

    # Прочитать аудиофайл и отправить его содержимое порциями.
    # with open(audio_file_name, 'rb') as f:
    while True:

        # get_new_frames
        data = reader.get_new_frames()
        while data is not None:
            yield stt_service_pb2.StreamingRecognitionRequest(audio_content=data)
            data = reader.get_new_frames()


def run(folder_id, iam_token, audio_file_name, reader, settings):
    # Установить соединение с сервером.
    cred = grpc.ssl_channel_credentials()
    channel = grpc.secure_channel('stt.api.cloud.yandex.net:443', cred)
    stub = stt_service_pb2_grpc.SttServiceStub(channel)

    # Отправить данные для распознавания.
    it = stub.StreamingRecognize(gen(folder_id, audio_file_name, reader, settings),
                                 metadata=(('authorization', 'Bearer %s' % iam_token),))

    # Обработать ответы сервера и вывести результат в консоль.
    try:
        for r in it:
            try:
                print('Start chunk: ')
                for alternative in r.chunks[0].alternatives:
                    print('alternative: ', alternative.text)
                print('Is final: ', r.chunks[0].final)
                print('')
            except LookupError:
                print('Not available chunks')
    except grpc._channel._Rendezvous as err:
        print('Error code %s, message: %s' % (err._state.code, err._state.details))


class AudioReaderThread(Thread):
    def __init__(self, settings):
        Thread.__init__(self)
        self.name = "audioReader"
        self.frames = Queue()

        self.CHUNK = settings["CHUNK"]
        self.FORMAT = settings["FORMAT"]
        self.CHANNELS = settings["CHANNELS"]
        self.RATE = settings["RATE"]
        self.RECORD_SECONDS = settings["record_second"]

        self.p = pyaudio.PyAudio()

        self.stream = self.p.open(format=self.FORMAT,
                                  channels=self.CHANNELS,
                                  rate=self.RATE,
                                  input=True,
                                  frames_per_buffer=self.CHUNK)

    def run(self):
        print("Record Started")
        while True:
            fr = self.stream.read(self.CHUNK)
            self.frames.put(fr)

    def get_new_frames(self):
        if not self.frames.empty():
            return self.frames.get()
        return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--token', required=True, help='IAM token')
    parser.add_argument('--folder_id', required=True, help='folder ID')
    parser.add_argument('--path', required=True, help='audio file path')
    args = parser.parse_args()
    settings = {
        "port": 5002,
        "record_second": 1,
        "CHUNK": 1024,  # Лучше делать побольше 10
        "FORMAT": pyaudio.paInt16,
        "CHANNELS": 1,
        "RATE": 16000
    }
    # run(args.folder_id, args.token, args.path)
    reader = AudioReaderThread(settings)
    reader.setDaemon(True)
    reader.start()

    run(args.folder_id, args.token, args.path, reader, settings)
