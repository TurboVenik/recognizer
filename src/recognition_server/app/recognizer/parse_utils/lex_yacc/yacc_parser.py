# Yacc example

import ply.yacc as yacc

from src.recognition_server.app.recognizer.parse_utils.lex_yacc.lex_parser import tokens


def p_expression_patient(p):
    'expression : PATIENT WORD WORD WORD'
    p[0] = {
        "type": "patient",
        "info": f"{p[2]} {p[3]} {p[4]}",
        "last": p[2],
        "first": p[3],
        "patronymic": p[4],

    }


def p_expression_start(p):
    '''
        expression : START
        | END
        | TABLET
        | SAVE
        | CANCEL
        | CLOSE
    '''
    p[0] = {
        "type": p.slice[1].type.lower(),
        "info": None,
    }


def p_expression_info(p):
    '''expression : VISIT NUMBER
    | DOCTOR words
    | NAME words
    | DOSE words
    | DURATION words
    | COMPLAINT words
    | DYNAMIC words
    | DESTINATIONS words
    | DISABILITY words'''
    p[0] = {
        "type": p.slice[1].type.lower(),
        "info": p[2],
    }


def p_words(p):
    '''
        words :  WORD
        | NUMBER
        | words WORD
        | words NUMBER
    '''


    if p.__len__() > 2:
        p[0] = f"{p[1]} {p[2]}"
    else:
        p[0] = p[1]
def p_error(p):
    pass
    # print(p)
    # print("\tSyntax error in input!")


# Build the parser
parser = yacc.yacc()

if __name__ == "__main__":
    result = parser.parse("справка нетрудоспособность 2 дня закончить запись")
    print(result)
    # while True:
    #     try:
    #         s = input('calc > ')
    #     except EOFError:
    #         break
    #     if not s: continue
    #     result = parser.parse(s)
    #     print(result)
