import ply.lex as lex

tokens = [
    'PATIENT',
    'START',

    'DOCTOR',
    'NUMBER',
    'VISIT',
    'TABLET',
    'END',
    'COMPLAINT',
    'DYNAMIC',
    'DESTINATIONS',
    'DISABILITY',

    'NAME',
    'DOSE',
    'DURATION',
    'SAVE',
    'CANCEL',

    'CLOSE',

    'WORD'
]


def t_KEY_PHRASE(t):
    r'законч[а-яА-Я]+\ запис[а-яА-Я]+'
    return None


def t_PATIENT(t):
    r'пациент[а-яА-Я]*'
    t.value = t.value
    return t


def t_START(t):
    r'начать\ при(е|ё)м[а-яА-Я]*|начать'
    t.value = t.value
    return t


def t_DOCTOR(t):
    r'специалист[а-яА-Я]*'
    t.value = t.value
    return t

def t_NUMBER(t):
    r'[0-9]+'
    t.value = int(t.value)
    return t

def t_VISIT(t):
    r'посещен[а-яА-Я]*'
    t.value = t.value
    return t


def t_TABLET(t):
    r'лекарств[а-яА-Я]*'
    t.value = t.value
    return t


def t_END(t):
    r'(заверш[а-яА-Я]*|законч[а-яА-Я]*)\ прием[а-яА-Я]*'
    t.value = t.value
    return t


def t_COMPLAINT(t):
    r'жалоб[а-яА-Я]*'
    t.value = t.value
    return t


def t_DYNAMIC(t):
    r'наблюден[а-яА-Я]*'
    t.value = t.value
    return t


def t_DESTINATIONS(t):
    r'назначен[а-яА-Я]*'
    t.value = t.value
    return t


def t_DISABILITY(t):
    r'справк[а-яА-Я]*'
    t.value = t.value
    return t


def t_NAME(t):
    r'назван[а-яА-Я]*'
    t.value = t.value
    return t


def t_DOSE(t):
    r'дозировк[а-яА-Я]*'
    t.value = t.value
    return t


def t_DURATION(t):
    r'продолжительн[а-яА-Я]*'
    t.value = t.value
    return t


def t_CLOSE(t):
    r'закрыт[а-яА-Я]*'
    t.value = t.value
    return t


def t_SAVE(t):
    r'сохран[а-яА-Я]*\ изменен[а-яА-Я]*'
    t.value = "сохранить"
    return t


def t_CANCEL(t):
    r'отмен[а-яА-Я]*\ изменен[а-яА-Я]*'
    t.value = "отмена"
    return t


def t_WORD(t):
    r'[а-яА-Я]+'
    t.value = t.value
    return t


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


t_ignore = ' \t'


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


lexer = lex.lex()

if __name__ == "__main__":
    data = "посещение 10"

    lexer.input(data)

    while True:
        tok = lexer.token()
        if not tok:
            break
        print(tok)
