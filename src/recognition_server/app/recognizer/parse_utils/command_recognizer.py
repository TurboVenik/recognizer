import time
from enum import Enum


class GrammarEnum(Enum):
    TEST = 0
    STATE_1 = 1
    STATE_2 = 2
    STATE_3 = 3
    STATE_4 = 4
    STATE_5 = 5


class CommandParseUtils(object):
    __instance = None

    @staticmethod
    def inst():
        return CommandParseUtils.__instance

    def __init__(self, voice_recognizer, text_recognizer):
        self.__voice_recognizer = voice_recognizer
        self.__text_recognizer = text_recognizer

    @staticmethod
    def init(voice_recognizer, text_recognizer):
        CommandParseUtils.__instance = CommandParseUtils(voice_recognizer, text_recognizer)

    def parse_bytes(self, buf, params):
        text_command = self.__voice_recognizer.recognize(buf, params.get("state"))
        print("\ttext command: ", text_command)
        if params == GrammarEnum.TEST:
            return {
                "type": "test",
                "text": text_command,
                "info": None
            }

        if text_command is None:
            return {
                "type": None,
                "text": None,
                "info": None
            }
        answer = self.__text_recognizer.parse_text(text_command)
        return answer

    def parse_post_text(self, buf, grammar_enum):
        text_command = buf.decode("utf-8")

        if grammar_enum == GrammarEnum.TEST:
            return {
                "type": "test",
                "text": text_command,
                "info": None
            }

        if text_command is None:
            return {
                "type": None,
                "text": None,
                "info": None
            }
        answer = self.__text_recognizer.parse_text(text_command)
        return answer
