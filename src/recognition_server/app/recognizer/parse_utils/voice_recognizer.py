import json
import os
import random
import string
import traceback
import urllib.request

import numpy as np

import soundfile as sf

from pocketsphinx import Decoder

from abc import abstractmethod


class AbstractRecognizeUtils:

    @abstractmethod
    def recognize(self, buf, settings=None):
        """Байты речи в текст"""


class SphinxRecognizeUtils(AbstractRecognizeUtils):

    """
    Класс для распознавания голоса с помощью сфинкса

    Attributes
    ----------
    __model_path : str
        путь до настроек сфинкса
    __decoder : Decoder
        сфинкс декодер
    __grammar_map : str[]
        грамматика
    __current_grammar : GrammarEnum
        текущая выбранная грамматика

    Methods
    -------
    use_grammar(grammar_enum)
        Выбор конкретной грамматики
    """

    def __init__(self, settings):
        self.__model_path = settings["model_path"]

        config = Decoder.default_config()
        config.set_string('-hmm', os.path.join(self.__model_path, 'zero_ru.cd_cont_4000'))
        config.set_string('-lm', os.path.join(self.__model_path, 'ru.lm'))
        config.set_string('-dict', os.path.join(self.__model_path, settings["dictionary"]))
        self.__decoder = Decoder(config)

        self.__grammar_map = [
            os.path.join(self.__model_path, settings["test_gram"]),
            os.path.join(self.__model_path, 'gram/state_1.jsgf'),
            os.path.join(self.__model_path, 'gram/state_2.jsgf'),
            os.path.join(self.__model_path, 'gram/state_3.jsgf'),
            os.path.join(self.__model_path, 'gram/state_4.jsgf'),
            os.path.join(self.__model_path, 'gram/state_5.jsgf')
        ]
        self.__current_grammar = None

    def __use_grammar(self, grammar_enum):
        print("use_grammar", grammar_enum)
        if self.__current_grammar != grammar_enum:
            self.__decoder.set_jsgf_file('grammar', self.__grammar_map[grammar_enum.value])
            self.__decoder.set_search('grammar')
            SphinxRecognizeUtils.current_grammar = grammar_enum

    def recognize(self, buf, grammar=None):
        self.__use_grammar(grammar)
        self.__decoder.start_utt()
        self.__decoder.process_raw(buf, False, False)
        self.__decoder.end_utt()

        print(self.__decoder.hyp())

        for item in self.__decoder.nbest():
            print(f"{item.hypstr}  {item.score}")
        print('Best hypothesis segments:',
              [(seg.word, seg.start_frame, seg.end_frame) for seg in self.__decoder.seg()])

        return self.__decoder.hyp().hypstr if self.__decoder.hyp() is not None else None


class YandexRecognitionUtils(AbstractRecognizeUtils):

    def __init__(self, settings):
        self.__folder_id = settings["folder_id"]
        self.__lang = settings["lang"]
        self.__topic = settings["topic"]
        self.__sample_rate_hertz = settings["sample_rate_hertz"]
        self.__i_am_token = None
        self.__update_token()
        self.__file_number = 0

    def __random_string(self, string_length=10):
        """Generate a random string of fixed length """
        letters = string.ascii_lowercase
        self.__file_number += 1
        return ''.join(random.choice(letters) for i in range(string_length)) + str(self.__file_number)

    def __convert_bytes_to_ogg(self, data_bytes):
        ogg_file_random_name = self.__random_string() + ".ogg"
        sample_rate = 16000
        # np.type can be: "float32", "int32", "int16", "uint8"
        # "int16" is working
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.io.wavfile.write.html
        dt = np.dtype("int16")
        dt = dt.newbyteorder('>')
        ogg_bytes = np.frombuffer(data_bytes, dtype=dt)
        sf.write(ogg_file_random_name, ogg_bytes, sample_rate)
        return ogg_file_random_name

    def recognize(self, buf, settings=None):
        file_name = self.__convert_bytes_to_ogg(buf)
        text_command = None
        with open(file_name, "rb") as file:
            try:
                data_ogg = file.read()

                text_command = self.speech_bytes_to_text(data_ogg)
            except Exception as e:
                print(f'{e}:\n', traceback.format_exc())
                try:
                    self.__update_token()
                    text_command = self.speech_bytes_to_text(data_ogg)
                except Exception as e:
                    print(f'{e}:\n', traceback.format_exc())

        os.remove(file_name)
        return text_command

    def speech_bytes_to_text(self, buf):
        params = "&".join([
            "topic=%s" % self.__topic,
            "folderId=%s" % self.__folder_id,
            "lang=%s" % self.__lang,
            "sample_rate_hertz=%s" % self.__sample_rate_hertz
        ])
        url = urllib.request.Request("https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?%s" % params, data=buf)

        url.add_header("Authorization", "Bearer %s" % self.__i_am_token)

        response_data = urllib.request.urlopen(url).read().decode('UTF-8')

        return json.loads(response_data).get("result")

    def __update_token(self):
        url = urllib.request.Request("https://iam.api.cloud.yandex.net/iam/v1/tokens",
                                     data=b'{\"yandexPassportOauthToken\":\"AgAAAAA1iCdbAATuwVkNBXl94EqYpmAgz1Ajzck\"}')
        url.add_header("Content-type", "application/x-www-form-urlencoded")

        self.__i_am_token = urllib.request.urlopen(url).read().decode('UTF-8')[16:863]


if __name__ == "__main__":
    with open("speech.wav", "rb") as f:
        data = f.read()
    recognizer_settings = {
        "folder_id": "b1gbo9epcqa0pmjknp6s",
        "lang": "ru-RU",
        "topic": "general",
        "sample_rate_hertz": 16000
    }
    y = YandexRecognitionUtils(recognizer_settings)
    answer = y.recognize(data)
    print(answer)
