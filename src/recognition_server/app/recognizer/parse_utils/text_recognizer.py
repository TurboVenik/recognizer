from abc import abstractmethod


class AbstractTextCommandParser:

    @abstractmethod
    def parse_text(self, text_command):
        pass


class LexYaccTextCommandParser(AbstractTextCommandParser):

    def __init__(self):
        self.word_to_int_map = {
            "один": 1,
            "два": 2,
            "три": 3,
            "четыре": 4,
            "пять": 5,
            "шесть": 6,
            "семь": 7,
            "восемь": 8,
            "девять": 9,
            "десять": 10
        }
        from src.recognition_server.app.recognizer.parse_utils.lex_yacc.yacc_parser import parser
        self.__parser = parser

    def parse_text(self, text_command):
        command_info = self.__parser.parse(text_command)
        if command_info is None:
            return {
                "type": None,
                "text": None,
                "info": None
            }
        command_info['text'] = text_command
        command_info['info'] = self.str_to_int(command_info['info'], command_info['type'])
        return command_info

    def str_to_int(self, string, command_type):

        if command_type == "visit" and string in self.word_to_int_map:
            return self.word_to_int_map[string]
        else:
            return string


class SimpleTextCommandParser(AbstractTextCommandParser):

    def __init__(self):
        self.command_map = {
            "пациент": "patient",
            "начать": "start",

            "специалист": "doctor",
            "посещение": "visit",
            "лекарство": "tablet",
            "анализ": "test",
            "жалоба": "complaint",
            "наблюдение": "dynamic",
            "назначение": "destinations",
            "справка": "disability",
            "завершить": "end",

            "название": "name",
            "дозировка": "dose",
            "продолжительность": "duration",
            "сохранить": "save",
            "отмена": "cancel",

            "закрыть": "close"
        }
        self.word_to_int_map = {
            "один": 1,
            "два": 2,
            "три": 3,
            "четыре": 4,
            "пять": 5,
            "шесть": 6,
            "семь": 7,
            "восемь": 8,
            "девять": 9,
            "десять": 10
        }

    def parse_text(self, text_command):
        words_arr = text_command.split(" ")

        answer = {
            "type": None,
            "text": None,
            "info": None
        }

        if text_command is None:
            return answer

        answer["text"] = text_command
        answer["type"] = self.command_map[words_arr[0]]
        answer["info"] = self.parse_info(words_arr, answer["type"])

        return answer

    def parse_info(self, words_arr, command_type):

        if words_arr.__len__() < 2:
            return None

        if command_type == "visit":
            return self.parse_words_to_int(words_arr[1:words_arr.__len__()])

        return ' '.join(words_arr[1: words_arr.__len__()])

    def parse_words_to_int(self, words_arr):
        return self.word_to_int_map[words_arr[0]]


if __name__ == "__main__":
    parser = LexYaccTextCommandParser()
    print(parser.parse_text("пациент иванов иван иванович"))
    print(parser.parse_text("начать"))
    print(parser.parse_text("завершить"))
    print(parser.parse_text("закончить"))
    print(parser.parse_text("посещение двести двадцать два"))
    print(parser.parse_text("специалист терапевт"))
    print(parser.parse_text("жалоба боль в правом колене"))
    print(parser.parse_text("справка нетрудоспособность десять дней"))
    print(parser.parse_text("лекарство"))
    print(parser.parse_text("название анальгин"))
    print(parser.parse_text("дозировка одна таблетка в день"))
    print(parser.parse_text("продолжительность две недели"))
    print(parser.parse_text("сохранить"))
    print(parser.parse_text("отмена"))
    print(parser.parse_text("закрыть"))
