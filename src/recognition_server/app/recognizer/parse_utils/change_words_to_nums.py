from src.recognition_server.app.recognizer.parse_utils.parse_text_numbers_to_numbers import ParseTextNumbersToNumbers


class Changer:

    @staticmethod
    def change(string):
        splitted_string = string.split(" ")
        newStr = ""
        numericStr = ""
        dic = ParseTextNumbersToNumbers.dictionary_keys(ParseTextNumbersToNumbers)
        for word in splitted_string:
            if word not in dic:
                if numericStr != "":
                    num = ParseTextNumbersToNumbers.word_to_num(numericStr)
                    newStr = newStr + " " + str(num)
                    numericStr = ""
                newStr = newStr + " " + word
            else:
                numericStr = numericStr + " " + word
        if numericStr != "":
            num = ParseTextNumbersToNumbers.word_to_num(numericStr)
            newStr = newStr + " " + str(num)
        newStr = newStr.strip(" ")
        return newStr


# if __name__ == "__main__":
#     print(Changer.change("два миллиона шесть тысяч двести семьдесят девять таблеток петру петровичу"))
#     print(Changer.change("два миллиона таблеток петру два миллиона шесть тысяч двести семьдесят девять петровичу"))
#     print(Changer.change("два таблеток петру петровичу два миллиона шесть тысяч двести семьдесят девять"))
#     print(Changer.change("двe таблеток два миллиона петру семь тысяч двести семьдесят девять петровичу два миллиона"))
#     print(Changer.change("посещение номер сто пять дата пятнадцатое февраля"))
