import json
import time

from flask import Blueprint, request

from src.recognition_server.app.recognizer.parse_utils.command_recognizer import CommandParseUtils, GrammarEnum

module = Blueprint('recognize', __name__, url_prefix='/rs')


@module.route('/recognize', methods=['POST'])
def post_recognize():
    d_time = time.time()
    audio_bytes = request.get_data()

    params = {
        "stop_phrase": request.values["stop_phrase"],
        "state": request.values["state"]
    }
    print("/rs/recognize")
    print("\t params:", params)
    print("\taudio audio_bytes[10]:", audio_bytes[0: 10])
    answer = CommandParseUtils.inst().parse_bytes(audio_bytes, params)
    response = json.dumps(answer, ensure_ascii=False)
    print("\tresponse", response)
    print("\trecognize time", time.time() - d_time)
    return response


@module.route('/text', methods=['POST'])
def post_text_recognize():
    print("StartRecognize...")
    answer = CommandParseUtils.inst().parse_post_text(request.get_data(), GrammarEnum[(request.values["grammar"])])
    print("EndRecognize...")
    print("response", json.dumps(answer, ensure_ascii=False))

    return json.dumps(answer, ensure_ascii=False)
