from src.recognition_server.app import create_app
from src.recognition_server.app.recognizer.parse_utils.command_recognizer import CommandParseUtils
from src.recognition_server.app.recognizer.parse_utils.text_recognizer import SimpleTextCommandParser, \
    LexYaccTextCommandParser
import argparse

from src.recognition_server.app.recognizer.parse_utils.voice_recognizer import SphinxRecognizeUtils, \
    YandexRecognitionUtils


def parse_command_line_params(edit_settings):
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", help="server port (default 5001)")
    parser.add_argument("-mp", "--model_path", help="location of voice model directory (default 'resources/model/')")
    parser.add_argument("-tg", "--test_gram", help="name of test gram (default 'test.jsgf')")
    parser.add_argument("-dict", "--dictionary", help="dictionary to use (default 'dic/ru.dic')")

    args = parser.parse_args()

    if args.port:
        edit_settings["port"] = int(args.port)
    if args.model_path:
        edit_settings["model_path"] = args.model_path
    if args.test_gram:
        edit_settings["test_gram"] = args.test_gram
    if args.dictionary:
        edit_settings["dictionary"] = args.dictionary


settings = {
    "port": 5001,
    "model_path": "resources/model/",
    "test_gram": "gram/test.jsgf",
    "dictionary": "dic/ru.dic",
    "folder_id": "b1gbo9epcqa0pmjknp6s",
    "lang": "ru-RU",
    "topic": "general",
    "sample_rate_hertz": 16000
}

parse_command_line_params(settings)

app = create_app()

# voice_recognizer = VoiceRecognizeUtils(settings)
voice_recognizer = YandexRecognitionUtils(settings)
text_recognizer = LexYaccTextCommandParser()

CommandParseUtils.init(voice_recognizer, text_recognizer)

# print(settings["model_path"], settings["test_gram"], settings["dictionary"])
if __name__ == '__main__':
    app.run(port=settings["port"])
